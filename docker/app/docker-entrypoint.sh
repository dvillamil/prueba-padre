#!/bin/sh
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- php-fpm "$@"
fi

if [ "$1" = 'php-fpm' ] || [ "$1" = 'bin/console' ]; then

	#composer global require "symfony/flex" --prefer-dist --no-progress --no-suggest --classmap-authoritative  --no-interaction;
    if [ "$APP_ENV" == 'int' ]; then
        # Always try to reinstall deps when not in prod
        composer install --no-dev --optimize-autoloader
        mkdir -p var/cache var/log var/session && chown -R www-data var
    elif [ "$APP_ENV" == 'prod' ]; then
        # Always try to reinstall deps when not in prod
        composer install --no-dev --optimize-autoloader
        mkdir -p var/cache var/log var/session && chown -R www-data var
    elif [ "$APP_ENV" != 'prod' ]; then
        # Always try to reinstall deps when not in prod
        composer clear-cache
        composer install --prefer-dist --no-progress --no-suggest --no-interaction

    fi
fi

exec docker-php-entrypoint "$@"
