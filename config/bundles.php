<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Knp\Bundle\GaufretteBundle\KnpGaufretteBundle::class => ['all' => true],
    Aws\Symfony\AwsBundle::class => ['all' => true],
    Uecode\Bundle\QPushBundle\UecodeQPushBundle::class => ['all' => true],
    Http\HttplugBundle\HttplugBundle::class => ['all' => true],
    AsResultados\OAMBundle\AsResultadosOAMBundle::class => ['all' => true],
];
