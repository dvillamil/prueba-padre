<?php
/**
 * Created by PhpStorm.
 * User: strick3r
 * Date: 11/12/18
 * Time: 20:30
 */

namespace App\Api\External\AsNews;


interface ResourceInterface
{
    public function addResource(string $id, array $item);
    public function getResource(string $id);

}