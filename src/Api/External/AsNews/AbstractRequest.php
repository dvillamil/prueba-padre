<?php

namespace App\Api\External\AsNews;

use AsResultados\OAMBundle\Api\QueryStringBuilder;
use AsResultados\OAMBundle\Model\IdInterface;
use AsResultados\OAMBundle\Api\AbstractRequest as AbstractRequestMaster;
use AsResultados\OAMBundle\Serializer\SerializerStandard;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

abstract class AbstractRequest extends AbstractRequestMaster
{
    public const VERSION = '/';
    public const API_GROUP = 'AsNews';

    /**
     * @var QueryStringBuilder
     */
    protected $query_string_builder;

    /**
     * Api constructor.
     * @param string $path
     */
    public function __construct($path)
    {
        parent::__construct(getenv('APP_API_AS_NEWS_HOST'), self::VERSION, $path);
        $this->query_string_builder = new QueryStringBuilder(self::EXTERNAL_API_GROUP . '/' . self::API_GROUP);
    }

    /**
     * @param array $parameters
     * @return string
     * @throws Exception
     */
    protected function getQueryString(array $parameters): string
    {
        $this->query_string_builder->clean();
        foreach ($parameters as $name => $value) {
            $this->query_string_builder->add($name, $value);
        }
        return $this->query_string_builder;
    }

    /**
     * Return array with all the ids from an IdInterface array
     * @param IdInterface[] $items
     * @return string[]
     */
    public function getIds(array $items): array
    {
        $ids = array();
        foreach ($items as $item) {
            $ids[] = $item->getId();
        }
        return $ids;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ExceptionInterface
     */
    protected function normalizeToJson(array $data)
    {
        $context = array();
        $context['skip_null_values'] = true;
        return SerializerStandard::getSerializer()->normalize($data, 'json', $context);
    }
}