<?php

namespace App\Api\External\AsNews;

interface RequestInterface
{
    public function getByProviderIds(string $provider, string $ids);
}