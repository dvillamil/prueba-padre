<?php

namespace App\Api\External\AsNews\Team;

use App\Api\External\AsNews\RequestInterface;

interface TeamInterface extends RequestInterface
{
    public const PATH = 'links.php';
}