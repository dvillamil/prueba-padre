<?php

namespace App\Api\External\AsNews\Team\Model\Embed;

class Image
{
    /**
     * @var string
     */
    private $id;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Image
     */
    public function setId(string $id): Image
    {
        $this->id = $id;
        return $this;
    }

}