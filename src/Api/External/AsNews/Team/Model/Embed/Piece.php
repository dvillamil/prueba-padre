<?php

namespace App\Api\External\AsNews\Team\Model\Embed;

class Piece
{
    private $id;

    /**
     * @return string
     */
    public function getId():string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Piece
     */
    public function setId(string $id): Piece
    {
        $this->id = $id;
        return $this;
    }

}