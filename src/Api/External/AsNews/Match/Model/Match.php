<?php

namespace App\Api\External\AsNews\Match\Model;

use App\Api\External\AsNews\Match\Model\Embed\Url;

class Match
{
    /**
     * @var string|null
     */
    private $id;

    /**
     * @var Url|null
     */
    private $url;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     */
    public function setId(?string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Url|null
     */
    public function getUrl(): ?Url
    {
        return $this->url;
    }

    /**
     * @param Url|null $url
     */
    public function setUrl(?Url $url): void
    {
        $this->url = $url;
    }
}