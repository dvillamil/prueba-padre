<?php

namespace App\Api\External\AsNews\Match\Model\Embed;

class Url
{
    /**
     * @var string|null
     */
    private $match;

    /**
     * @var News[]|null
     */
    private $news;

    public function __construct()
    {
    }

    /**
     * @return string|null
     */
    public function getMatch(): ?string
    {
        return $this->match;
    }

    /**
     * @param string|null $match
     */
    public function setMatch(?string $match): void
    {
        $this->match = $match;
    }

    /**
     * @return News[]|null
     */
    public function getNews(): ?array
    {
        return $this->news;
    }

    /**
     * @param News[]|null $news
     */
    public function setNews(?array $news): void
    {
        $this->news = $news;
    }
}