<?php

namespace App\Api\External\AsNews\Match\Model\Embed;

class News
{
    /**
     * @var string|null
     */
    private $editionSlug;

    /**
     * @var string|null
     */
    private $preMatch;

    /**
     * @var string|null
     */
    private $live;

    /**
     * @var string|null
     */
    private $finished;

    public function __construct()
    {
    }

    /**
     * @return string|null
     */
    public function getEditionSlug(): ?string
    {
        return $this->editionSlug;
    }

    /**
     * @param string|null $editionSlug
     */
    public function setEditionSlug(?string $editionSlug): void
    {
        $this->editionSlug = $editionSlug;
    }

    /**
     * @return string|null
     */
    public function getPreMatch(): ?string
    {
        return $this->preMatch;
    }

    /**
     * @param string|null $preMatch
     */
    public function setPreMatch(?string $preMatch): void
    {
        $this->preMatch = $preMatch;
    }

    /**
     * @return string|null
     */
    public function getLive(): ?string
    {
        return $this->live;
    }

    /**
     * @param string|null $live
     */
    public function setLive(?string $live): void
    {
        $this->live = $live;
    }

    /**
     * @return string|null
     */
    public function getFinished(): ?string
    {
        return $this->finished;
    }

    /**
     * @param string|null $finished
     */
    public function setFinished(?string $finished): void
    {
        $this->finished = $finished;
    }
}