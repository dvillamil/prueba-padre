<?php

namespace App\Api\External\AsNews\Match;

use App\Api\External\AsNews\RequestInterface;

interface MatchInterface extends RequestInterface
{
    public const PATH = 'links.php';
}