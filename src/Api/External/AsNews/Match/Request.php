<?php

namespace App\Api\External\AsNews\Match;

use App\Api\External\AsNews\AbstractRequest;
use App\Api\External\AsNews\AbstractResource;
use App\Api\External\AsNews\Match\Model\Match;
use App\Utils\ArrayTrait;
use Exception;
use Psr\Http\Message\RequestInterface;

final class Request extends AbstractRequest implements MatchInterface
{
    const TYPE = 'games';

    use ArrayTrait;

    public function __construct()
    {
        parent::__construct(self::PATH);
    }

    /**
     * @param string $provider
     * @param string $ids
     * @return RequestInterface
     * @throws Exception
     */
    public function getByProviderIds(string $provider, string $ids): RequestInterface
    {
        $parameters = [
            'provider' => strtolower($provider),
            'type' => self::TYPE,
            'ids' => $ids
        ];

        return $this->request_factory->createRequest(
            'GET',
            $this->getUrl() . '?' . $this->getQueryString($parameters));
    }

    /**
     * @param Match[] $team
     * @return RequestInterface
     * @throws Exception
     */
    public function post(array $team): RequestInterface
    {
        throw new Exception('Post request from batch is not allowed for Match AsNews Resources');
    }

    /**
     * @param Match[] $team
     * @return RequestInterface
     * @throws Exception
     */
    public function patch(array $team): RequestInterface
    {
        throw new Exception('Patch request from batch is not allowed for Match AsNews Resources');
    }

    /**
     * @param Match[] $team
     * @return RequestInterface
     * @throws Exception
     */
    public function put(array $team): RequestInterface
    {
        throw new Exception('Put request from batch is not allowed for Match AsNews Resources');
    }

    /**
     * @param string[] $teamIds
     * @return RequestInterface[]
     * @throws Exception
     */
    public function delete(array $teamIds): array
    {
        throw new Exception('\Delete request from batch is not allowed for Match AsNews Resources');
    }

    /**
     * @return AbstractResource
     */
    public function getResource(): AbstractResource
    {
        return new Resource();
    }
}