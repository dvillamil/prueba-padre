<?php

namespace App\Api\External\AsNews\Player;

use App\Api\External\AsNews\AbstractRequest;
use App\Api\External\AsNews\AbstractResource;
use App\Api\External\AsNews\Player\Model\Player;
use App\Utils\ArrayTrait;
use Exception;
use Psr\Http\Message\RequestInterface;

final class Request extends AbstractRequest implements PlayerInterface
{
    const TYPE = 'players';

    use ArrayTrait;

    public function __construct()
    {
        parent::__construct(self::PATH);
    }

    /**
     * @param string $provider
     * @param string $ids
     * @return RequestInterface
     * @throws Exception
     */
    public function getByProviderIds(string $provider, string $ids): RequestInterface
    {
        $parameters = [
            'provider' => strtolower($provider),
            'type' => self::TYPE,
            'ids' => $ids
        ];
        return $this->request_factory->createRequest(
            'GET',
            $this->getUrl() . '?' . $this->getQueryString($parameters));
    }

    /**
     * @param Player[] $player
     * @return RequestInterface
     * @throws Exception
     */
    public function post(array $player): RequestInterface
    {
        throw new Exception('Post request from batch is not allowed for Player AsNews Resources');
    }

    /**
     * @param Player[] $player
     * @return RequestInterface
     * @throws Exception
     */
    public function patch(array $player): RequestInterface
    {
        throw new Exception('Patch request from batch is not allowed for Player AsNews Resources');
    }

    /**
     * @param Player[] $player
     * @return RequestInterface
     * @throws Exception
     */
    public function put(array $player): RequestInterface
    {
        throw new Exception('Put request from batch is not allowed for Player AsNews Resources');
    }

    /**
     * @param string[] $playerIds
     * @return RequestInterface[]
     * @throws Exception
     */
    public function delete(array $playerIds): array
    {
        throw new Exception('\Delete request from batch is not allowed for Player AsNews Resources');
    }

    /**
     * @return AbstractResource
     */
    public function getResource(): AbstractResource
    {
        return new Resource();
    }
}