<?php

namespace App\Api\External\AsNews\Player;

use App\Api\External\AsNews\AbstractResource;
use App\Api\External\AsNews\Player\Model\Embed\Image;
use App\Api\External\AsNews\Player\Model\Embed\Piece;
use App\Api\External\AsNews\Player\Model\Player;
use App\Api\External\AsNews\ResourceInterface;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

final class Resource extends AbstractResource implements ResourceInterface
{
    private $resource;

    /**
     * @inheritDoc
     * @return Player
     */
    public function deserializeOne(array $json, $object = null): Player
    {
        if (is_null($object)) {
            $player = new Player();
        } else {
            $player = $object;
        }
        if (!empty($json['tag'])) {
            $player->setTag($json['tag']);
        }
        if (!empty($json['nombre_normalizado'])) {
            $player->setSlug($json['nombre_normalizado']);
        }
        if (!empty($json['nombre_corto'])) {
            $player->setShortName($json['nombre_corto']);
        }
        if (isset($json['img']) && !empty($json['img'])) {
            $image = new Image();
            $image->setId($json['id']);
            $player->setImage($image);
        }
        if (isset($json['url']) && !empty($json['url'])) {
            $piece = new Piece();
            $piece->setId($json['url']);
            $player->setPiece($piece);
        }
        return $player;
    }

    /**
     * @inheritDoc
     * @return Player[]
     */
    public function deserializeMultiple(array $json, ?array &$objects = null): array
    {
        return parent::deserializeMultiple($json, $objects);
    }

    /**
     * @param string $id
     * @param array $item
     * @throws ExceptionInterface
     */
    public function addResource(string $id, array $item)
    {
        $this->resource[$id] = $this->deserializeOne($item);
    }

    /**
     * @param string $id
     * @return Player
     */
    public function getResource(string $id): ?Player
    {
        $cleanId = filter_var($id, FILTER_SANITIZE_NUMBER_INT);
        if (isset($this->resource[$cleanId])) {
            return $this->resource[$cleanId];
        }
        return null;
    }
}