<?php

namespace App\Processor\Provider\Traits;

use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Model\Results\Team\Team;
use AsResultados\OAMBundle\Api\Internal\Results\Team\Request as TeamRequest;
use AsResultados\OAMBundle\Exception\MissingItemException;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

trait TeamTrait
{
    /**
     * @var Team
     */
    private $team;

    /**
     * @return Team
     */
    public function getTeam(): Team
    {
        return $this->team;
    }

    /**
     * @param string $teamId
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setTeam(string $teamId, ClientManager $client): void
    {
        try {
            $teamRequest = new TeamRequest();
            $request = $teamRequest->getById($teamId);
            $client->clearRequests();
            $client->addRequest($request);
            $client->execute();
            $response = $client->getResponseFor(
                $request,
                [$teamRequest::STATUS_CODE_OK, $teamRequest::STATUS_CODE_NOT_FOUND]
            );
            $resource = $teamRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()])) {
                throw new MissingItemException(
                    $teamId,
                    Team::class,
                    $teamRequest->getUrl()
                );
            }
            $team = $resource->deserializeOne(
                $response[$resource->getResponseItemsName()]
            );
            $this->team = $team;
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find team with id: ' . $teamId . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }
}