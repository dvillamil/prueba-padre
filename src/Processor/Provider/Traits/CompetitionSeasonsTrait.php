<?php

namespace App\Processor\Provider\Traits;

use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Api\Internal\Results\CompetitionSeason\Request as CompetitionSeasonRequest;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Request as MatchRequest;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeason;
use AsResultados\OAMBundle\Model\Results\Match\Match;
use AsResultados\OAMBundle\Exception\MissingItemException;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

trait CompetitionSeasonsTrait
{
    /**
     * @var CompetitionSeason[]
     */
    private $competitionSeasons;

    /**
     * @var int[]
     */
    private $competitionSeasonsMatchesInserted;

    /**
     * @var Match[]
     */
    private $matches = [];

    /**
     * idStage => idCompetitionSeason
     * @var string[]
     */
    private $stageCompetitionSeason;

    /**
     * @var string[]
     */
    private $competitionSeasonsIdsStatusPlaying = array();

    /**
     * @return CompetitionSeason[]
     */
    protected function getCompetitionSeasons(): array
    {
        return $this->competitionSeasons;
    }

    /**
     * @return string[]
     */
    protected function getCompetitionSeasonsIds(): array
    {
        return array_keys($this->competitionSeasons);
    }

    /**
     * @param string $id
     * @return CompetitionSeason
     * @throws MissingItemException
     */
    protected function getCompetitionSeason(string $id): CompetitionSeason
    {
        if (!isset($this->competitionSeasons[$id])) {
            throw new MissingItemException($id, CompetitionSeason::class, 'CompetitionSeasonsTrait');
        }
        return $this->competitionSeasons[$id];
    }

    /**
     * @return int[]
     */
    protected function getCompetitionSeasonsMatchesInserted(): array
    {
        return $this->competitionSeasonsMatchesInserted;
    }

    /**
     * @param string $id
     * @return int
     * @throws MissingItemException
     */
    protected function getCompetitionSeasonMatchesInserted(string $id): int
    {
        if (!isset($this->competitionSeasonsMatchesInserted[$id])) {
            throw new MissingItemException($id, 'competitionSeasonsMatchesInserted', 'CompetitionSeasonsTrait');
        }
        return $this->competitionSeasonsMatchesInserted[$id];
    }

    /**
     * @return Match[]
     */
    protected function getMatches(): array
    {
        return $this->matches;
    }

    /**
     * @param string $id
     * @return Match
     * @throws MissingItemException
     */
    protected function getMatch(string $id): Match
    {
        if (!isset($this->matches[$id])) {
            throw new MissingItemException($id, Match::class, 'CompetitionSeasonsTrait');
        }
        return $this->matches[$id];
    }

    /**
     * @param string $id
     * @return string
     * @throws MissingItemException
     */
    protected function getCompetitionSeasonIdFromStageId(string $id): string
    {
        if (!isset($this->stageCompetitionSeason[$id])) {
            throw new MissingItemException($id, 'stage id', 'CompetitionSeasonsTrait in stageToCompetitionSeason');
        }
        return $this->stageCompetitionSeason[$id];
    }

    /**
     * @param string[] $competitionSeasonIds
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setCompetitionSeasons(array $competitionSeasonIds, ClientManager $client): void
    {
        try {
            if (empty($competitionSeasonIds)) {
                throw new EmptyItemException('array competitionSeasonIds', __METHOD__);
            }
            $competitionSeasonRequest = new CompetitionSeasonRequest();
            $requests = $competitionSeasonRequest->getByIds($competitionSeasonIds);
            $client->clearRequests();
            $client->addRequests($requests);
            $client->execute();
            $response = $client->getResponseForMultiple(
                $requests,
                [$competitionSeasonRequest::STATUS_CODE_OK, $competitionSeasonRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $competitionSeasonRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()]) || empty($response[$resource->getResponseItemsName()])) {
                throw new MissingItemException(
                    implode(',', $competitionSeasonIds),
                    CompetitionSeason::class,
                    $competitionSeasonRequest->getUrl()
                );
            }
            $competitionSeasons = $resource->deserializeMultiple(
                $response[$resource->getResponseItemsName()]
            );
            foreach ($competitionSeasons as $competitionSeason) {
                $this->competitionSeasons[$competitionSeason->getId()] = $competitionSeason;
                if (!is_null($competitionSeason->getStatus()) &&
                    $competitionSeason->getStatus()->getId() === CategoryInterface::COMPETITION_SEASON_STATUS_PLAYING) {
                    $this->competitionSeasonsIdsStatusPlaying[] = $competitionSeason->getId();
                }
                if (is_array($competitionSeason->getStages())) {
                    foreach ($competitionSeason->getStages() as $stage) {
                        $this->stageCompetitionSeason[$stage->getId()] = $competitionSeason->getId();
                    }
                }
            }
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find competitionSeasons with ids: ' . implode(',', $competitionSeasonIds) . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }

    /**
     * @param string[] $stagesIds
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setCompetitionSeasonsByStageIds(array $stagesIds, ClientManager $client): void
    {
        try {
            if (empty($stagesIds)) {
                throw new EmptyItemException('array stagesIds', __METHOD__);
            }
            $competitionSeasonRequest = new CompetitionSeasonRequest();
            $requestCompetitionSeason = $competitionSeasonRequest->getByStagesIncludedStages($stagesIds);
            $client->clearRequests();
            $client->addRequests($requestCompetitionSeason);
            $client->execute(true);
            $response = $client->getResponseForMultiple(
                $requestCompetitionSeason,
                [$competitionSeasonRequest::STATUS_CODE_OK, $competitionSeasonRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $competitionSeasonRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()]) || empty($response[$resource->getResponseItemsName()])) {
                throw new MissingItemException(
                    implode(',', $stagesIds),
                    CompetitionSeason::class,
                    $competitionSeasonRequest->getUrl()
                );
            }
            $competitionSeasons = $resource->deserializeMultiple(
                $response[$resource->getResponseItemsName()]
            );
            foreach ($competitionSeasons as $competitionSeason) {
                $this->competitionSeasons[$competitionSeason->getId()] = $competitionSeason;
                if (!is_null($competitionSeason->getStatus()) &&
                    $competitionSeason->getStatus()->getId() === CategoryInterface::COMPETITION_SEASON_STATUS_PLAYING) {
                    $this->competitionSeasonsIdsStatusPlaying[] = $competitionSeason->getId();
                }
                if (is_array($competitionSeason->getStages())) {
                    foreach ($competitionSeason->getStages() as $stage) {
                        $this->stageCompetitionSeason[$stage->getId()] = $competitionSeason->getId();
                    }
                }
            }
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find competitionSeason with stages: ' . implode(',', $stagesIds) . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }

    /**
     * @param string[] $competitionSeasonIds
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setCompetitionSeasonsAndNumberOfMatchesInsertedAndDetails(array $competitionSeasonIds, ClientManager $client): void
    {
        try {
            if (empty($competitionSeasonIds)) {
                throw new EmptyItemException('array competitionSeasonIds', __METHOD__);
            }
            $matchRequest = new MatchRequest();
            $requestMatches = $matchRequest->getByCompetitionSeasonsFieldsIdDetail($competitionSeasonIds);
            $competitionSeasonRequest = new CompetitionSeasonRequest();
            $requests = $competitionSeasonRequest->getByIds($competitionSeasonIds);
            $client->clearRequests();
            $client->addRequests($requests);
            $client->addRequest($requestMatches);
            $client->execute(true);
            $response = $client->getResponseForMultiple(
                $requests,
                [$competitionSeasonRequest::STATUS_CODE_OK, $competitionSeasonRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $competitionSeasonRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()]) || empty($response[$resource->getResponseItemsName()])) {
                throw new MissingItemException(
                    implode(',', $competitionSeasonIds),
                    CompetitionSeason::class,
                    $competitionSeasonRequest->getUrl()
                );
            }
            $competitionSeasons = $resource->deserializeMultiple(
                $response[$resource->getResponseItemsName()]
            );
            foreach ($competitionSeasons as $competitionSeason) {
                $this->competitionSeasons[$competitionSeason->getId()] = $competitionSeason;
                if (!is_null($competitionSeason->getStatus()) &&
                    $competitionSeason->getStatus()->getId() === CategoryInterface::COMPETITION_SEASON_STATUS_PLAYING) {
                    $this->competitionSeasonsIdsStatusPlaying[] = $competitionSeason->getId();
                }
                $this->competitionSeasonsMatchesInserted[$competitionSeason->getId()] = 0;
                if (is_array($competitionSeason->getStages())) {
                    foreach ($competitionSeason->getStages() as $stage) {
                        $this->stageCompetitionSeason[$stage->getId()] = $competitionSeason->getId();
                    }
                }
            }
            $response = $client->getResponseFor(
                $requestMatches,
                [$matchRequest::STATUS_CODE_OK, $matchRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $matchRequest->getResource();
            $matches = $resource->deserializeMultiple(
                $response[$resource->getResponseItemsName()]
            );
            foreach ($matches as $match) {
                if ($match instanceof Match) {
                    $this->matches[$match->getId()] = $match;
                    $this->competitionSeasonsMatchesInserted[$match->getCompetitionSeason()->getId()]++;
                }
            }
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find competitionSeasons with ids: ' . implode(',', $competitionSeasonIds) . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }

    /**
     * @param string[] $stagesIds
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setCompetitionSeasonsAndNumberOfMatchesInsertedAndDetailsByStageIds(array $stagesIds, ClientManager $client): void
    {
        try {
            if (empty($stagesIds)) {
                throw new EmptyItemException('array stagesIds', __METHOD__);
            }
            $competitionSeasonRequest = new CompetitionSeasonRequest();
            $requestCompetitionSeason = $competitionSeasonRequest->getByStagesIncludedStages($stagesIds);
            $matchRequest = new MatchRequest();
            $requestMatches = $matchRequest->getByStagesFieldsIdDetailCompetitionSeasonAndStage($stagesIds);
            $client->clearRequests();
            $client->addRequests($requestCompetitionSeason);
            $client->addRequest($requestMatches);
            $client->execute(true);
            $response = $client->getResponseForMultiple(
                $requestCompetitionSeason,
                [$competitionSeasonRequest::STATUS_CODE_OK, $competitionSeasonRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $competitionSeasonRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()]) || empty($response[$resource->getResponseItemsName()])) {
                throw new MissingItemException(
                    implode(',', $stagesIds),
                    CompetitionSeason::class,
                    $competitionSeasonRequest->getUrl()
                );
            }
            $competitionSeasons = $resource->deserializeMultiple(
                $response[$resource->getResponseItemsName()]
            );
            foreach ($competitionSeasons as $competitionSeason) {
                $this->competitionSeasons[$competitionSeason->getId()] = $competitionSeason;
                if (!is_null($competitionSeason->getStatus()) &&
                    $competitionSeason->getStatus()->getId() === CategoryInterface::COMPETITION_SEASON_STATUS_PLAYING) {
                    $this->competitionSeasonsIdsStatusPlaying[] = $competitionSeason->getId();
                }
                $this->competitionSeasonsMatchesInserted[$competitionSeason->getId()] = 0;
                if (is_array($competitionSeason->getStages())) {
                    foreach ($competitionSeason->getStages() as $stage) {
                        $this->stageCompetitionSeason[$stage->getId()] = $competitionSeason->getId();
                    }
                }
            }
            $response = $client->getResponseFor(
                $requestMatches,
                [$matchRequest::STATUS_CODE_OK, $matchRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $matchRequest->getResource();
            $matches = $resource->deserializeMultiple(
                $response[$resource->getResponseItemsName()]
            );
            foreach ($matches as $match) {
                if ($match instanceof Match) {
                    $this->matches[$match->getId()] = $match;
                    $this->competitionSeasonsMatchesInserted[$match->getCompetitionSeason()->getId()]++;
                }
            }
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find competitionSeason with stages: ' . implode(',', $stagesIds) . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }

    /**
     * @return string[]
     */
    public function getCompetitionSeasonsIdsStatusPlaying(): array
    {
        return $this->competitionSeasonsIdsStatusPlaying;
    }
}