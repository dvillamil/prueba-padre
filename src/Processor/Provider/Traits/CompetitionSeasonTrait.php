<?php

namespace App\Processor\Provider\Traits;

use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Api\Internal\Results\CompetitionSeason\Request as CompetitionSeasonRequest;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeason;
use AsResultados\OAMBundle\Model\Results\Match\Match;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Request as MatchRequest;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

trait CompetitionSeasonTrait
{
    /**
     * @var CompetitionSeason
     */
    private $competitionSeason;

    /**
     * @var int
     */
    private $competitionSeasonMatchesInserted;

    /**
     * @var Match[]
     */
    private $matches = [];

    /**
     * @return CompetitionSeason
     */
    protected function getCompetitionSeason(): CompetitionSeason
    {
        return $this->competitionSeason;
    }

    /**
     * @return int
     */
    public function getCompetitionSeasonMatchesInserted(): int
    {
        return $this->competitionSeasonMatchesInserted;
    }

    /**
     * @return Match[]
     */
    public function getMatches(): array
    {
        return $this->matches;
    }

    /**
     * @param string $competitionSeasonId
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setCompetitionSeason(string $competitionSeasonId, ClientManager $client): void
    {
        try {
            $competitionSeasonRequest = new CompetitionSeasonRequest();
            $request = $competitionSeasonRequest->getById($competitionSeasonId);
            $client->clearRequests();
            $client->addRequest($request);
            $client->execute();
            $response = $client->getResponseFor(
                $request,
                [$competitionSeasonRequest::STATUS_CODE_OK, $competitionSeasonRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $competitionSeasonRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()])) {
                throw new MissingItemException(
                    $competitionSeasonId,
                    CompetitionSeason::class,
                    $competitionSeasonRequest->getUrl()
                );
            }
            $competitionSeason = $resource->deserializeOne(
                $response[$resource->getResponseItemsName()]
            );
            $this->competitionSeason = $competitionSeason;
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find competitionSeason with id: ' . $competitionSeasonId . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }

    /**
     * @param string $competitionSeasonId
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setCompetitionSeasonAndNumberOfMatchesInsertedAndDetails(string $competitionSeasonId, ClientManager $client): void
    {
        try {
            $matchRequest = new MatchRequest();
            $requestMatches = $matchRequest->getByCompetitionSeasonFieldsIdDetail($competitionSeasonId);
            $competitionSeasonRequest = new CompetitionSeasonRequest();
            $request = $competitionSeasonRequest->getById($competitionSeasonId);
            $client->clearRequests();
            $client->addRequest($request);
            $client->addRequest($requestMatches);
            $client->execute(true);
            $response = $client->getResponseFor(
                $request,
                [$competitionSeasonRequest::STATUS_CODE_OK, $competitionSeasonRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $competitionSeasonRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()])) {
                throw new MissingItemException(
                    $competitionSeasonId,
                    CompetitionSeason::class,
                    $competitionSeasonRequest->getUrl()
                );
            }
            $competitionSeason = $resource->deserializeOne(
                $response[$resource->getResponseItemsName()]
            );
            $this->competitionSeason = $competitionSeason;
            $response = $client->getResponseFor(
                $requestMatches,
                [$matchRequest::STATUS_CODE_OK, $matchRequest::STATUS_CODE_NOT_FOUND]);
            $resource = $matchRequest->getResource();
            $this->competitionSeasonMatchesInserted = count($response[$resource->getResponseItemsName()]);
            $matches = $resource->deserializeMultiple(
                $response[$resource->getResponseItemsName()]
            );
            foreach ($matches as $match) {
                if ($match instanceof Match) {
                    $this->matches[$match->getId()] = $match;
                }
            }
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find competitionSeason with id: ' . $competitionSeasonId . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }
}