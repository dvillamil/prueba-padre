<?php

namespace App\Processor\Provider\Traits;

use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Model\Results\Broadcast\Broadcast;
use AsResultados\OAMBundle\Api\Internal\Results\Broadcast\Request as BroadcastRequest;
use AsResultados\OAMBundle\Exception\MissingItemException;
use Exception;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

trait BroadcastTrait
{
    /**
     * @var Broadcast|null
     */
    private $broadcast;

    /**
     * @return Broadcast|null
     */
    public function getBroadcast(): ?Broadcast
    {
        return $this->broadcast;
    }

    /**
     * @param string $broadcastId
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setBroadcast(string $broadcastId, ClientManager $client): void
    {
        try {
            $broadcastRequest = new BroadcastRequest();
            $request = $broadcastRequest->getById($broadcastId);
            $client->clearRequests();
            $client->addRequest($request);
            $client->execute();
            $response = $client->getResponseFor(
                $request,
                [$broadcastRequest::STATUS_CODE_OK, $broadcastRequest::STATUS_CODE_NOT_FOUND]
            );
            $resource = $broadcastRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()])) {
                throw new MissingItemException(
                    $broadcastId,
                    Broadcast::class,
                    $broadcastRequest->getUrl()
                );
            }
            $broadcast = $resource->deserializeOne(
                $response[$resource->getResponseItemsName()]
            );
            $this->broadcast = $broadcast;
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find broadcast with id: ' . $broadcastId . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }

    /**
     * @param string $match
     * @param string $language
     * @param ClientManager $client
     * @throws Exception
     */
    protected function setBroadcastByMatchAndLanguage(string $match, string $language, ClientManager $client): void
    {
        try {
            $broadcastRequest = new BroadcastRequest();
            $request = $broadcastRequest->getByMatchAndLanguage($match, $language);
            $client->clearRequests();
            $client->addRequest($request);
            $client->execute();
            $response = $client->getResponseFor(
                $request,
                [$broadcastRequest::STATUS_CODE_OK, $broadcastRequest::STATUS_CODE_NOT_FOUND]
            );
            $resource = $broadcastRequest->getResource();
            if (!isset($response[$resource->getResponseItemsName()][0]) || empty($response[$resource->getResponseItemsName()][0])) {
                throw new MissingItemException(
                    "match: " . $match . ' - language: ' . $language,
                    Broadcast::class,
                    $broadcastRequest->getUrl()
                );
            }
            $broadcast = $resource->deserializeOne(
                $response[$resource->getResponseItemsName()][0]
            );
            $this->broadcast = $broadcast;
        } catch (ExceptionInterface $e) {
        } catch (Exception $e) {
            throw new Exception('Can not find broadcast with match: ' . $match . ' - language: ' . $language . ' - ' . $e->getMessage());
        } finally {
            $client->clearRequests();
        }
    }
}