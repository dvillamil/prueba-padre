<?php

namespace App\Processor\Provider;

use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\Request as MappingRequest;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\Resource as MappingResource;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Client\ClientManager;
use AsResultados\OAMBundle\Exception\MappingBucketException;
use AsResultados\OAMBundle\Log\Logger;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Exception\MappingException;
use Exception;
use Gaufrette\Filesystem;
use Http\Client\Common\Exception\BatchException;
use App\Processor\Provider\Traits\EventDispatcherTrait;
use Symfony\Component\Serializer\Exception\ExceptionInterface;

abstract class AbstractProcessor implements ProcessorInterface, EventDispatcherDependencyInterface
{
    use EventDispatcherTrait;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var ClientManager
     */
    private $client;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * AbstractProcessor constructor.
     * @param Filesystem $fileSystem
     * @param ClientManager $client
     * @param Logger $logger
     * @param String $filePath
     * @throws Exception
     */
    public function __construct(Filesystem $fileSystem, ClientManager $client, Logger $logger, string $filePath)
    {
        $this->fileSystem = $fileSystem;
        $this->client = $client;
        $this->logger = $logger;
        $this->filePath = $filePath;
    }

    /**
     * @inheritDoc
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @return Filesystem
     */
    protected function getFileSystem(): Filesystem
    {
        return $this->fileSystem;
    }

    /**
     * @return ClientManager
     */
    protected function getClient(): ClientManager
    {
        return $this->client;
    }

    /**
     * @return Logger
     */
    protected function getLogger(): Logger
    {
        return $this->logger;
    }

    /**
     * Return all the mappings we need to search for
     * @return ProviderIdsCollection[]
     * @throws Exception
     */
    protected abstract function getMappingsFromProviderData(): array;

    /**
     * @param ProviderIdsCollection[] $mappings
     * @throws Exception
     */
    final private function setMappings(array $mappings): void
    {
        if (empty($mappings)) {
            return;
        }
        $mappingCollection = MappingCollection::getInstance();
        try {
            $this->getClient()->clearRequests();
            $mappingRequest = new MappingRequest();
            $mappingResource = new MappingResource();
            $requests = array();
            foreach ($mappings as $mapping) {
                if ($mapping->isAll()) {
                    $request = $mappingRequest->getByProviderAndType($mapping->getProvider(), $mapping->getType());
                    $this->getClient()->addRequest($request);
                } else {
                    $request = $mappingRequest->getByIdsAndProviderAndType(
                        $mapping->getIds(), $mapping->getProvider(), $mapping->getType()
                    );
                    $this->getClient()->addRequests($request);
                }
                $requests[] = $request;
            }
            $this->getClient()->execute(true);
            foreach ($requests as $request) {
                if (is_array($request)) {
                    $response = $this->getClient()->getResponseForMultiple($request, [$mappingRequest::STATUS_CODE_OK]);
                } else {
                    $response = $this->getClient()->getResponseFor($request, [$mappingRequest::STATUS_CODE_OK]);
                }
                $responseMappings = $mappingResource->deserializeMultiple($response[$mappingResource->getResponseItemsName()]);
                $mappingCollection->addMultiple($responseMappings);
            }
        } catch (BatchException $e) {
            $exceptionMsgs = array();
            $exceptions = $e->getResult()->getExceptions();
            foreach ($exceptions as $exception) {
                /** @var Exception $exception */
                $exceptionMsgs[] = $exception->getMessage();
            }
            throw new Exception('Mappings can not be initialized: ' . implode(', ', $exceptionMsgs));
        } catch (Exception | ExceptionInterface $e) {
            throw new Exception('Mappings can not be initialized: ' . $e->getMessage());
        } finally {
            $this->getClient()->clearRequests();
        }
    }

    /**
     * @param RequestsResourceItem[] $requestResourceItems
     * @throws Exception
     */
    final protected function setMappingsOwn(array $requestResourceItems): void
    {
        if (empty($requestResourceItems)) {
            return;
        }
        $mappingCollection = MappingCollection::getInstance();
        try {
            $this->getClient()->clearRequests();
            foreach ($requestResourceItems as $requestResourceItem) {
                if (count($requestResourceItem->getRequests()) === 1) {
                    $this->getClient()->addRequest(current($requestResourceItem->getRequests()));
                } else {
                    $this->getClient()->addRequests($requestResourceItem->getRequests());
                }
            }
            $this->getClient()->execute(true);
            foreach ($requestResourceItems as $requestResourceItem) {
                $resource = $requestResourceItem->getResource();
                if (count($requestResourceItem->getRequests()) === 1) {
                    $response = $this->getClient()->getResponseFor(current($requestResourceItem->getRequests()), [$resource::STATUS_CODE_OK]);
                } else {
                    $response = $this->getClient()->getResponseForMultiple($requestResourceItem->getRequests(), [$resource::STATUS_CODE_OK]);
                }
                $mappings = $resource->deserializeMultiple(
                    $response[$resource->getResponseItemsName()]
                );
                $mappingCollection->addOwnMultiple($mappings);
            }
        } catch (Exception | ExceptionInterface $e) {
            throw new Exception('Own mappings can not be initialized: ' . $e->getMessage());
        } finally {
            $this->getClient()->clearRequests();
        }
    }

    /**
     * @inheritDoc
     * @throws MappingException
     * @throws ExceptionInterface
     * @throws Exception
     */
    final public function initialize(): void
    {
        $mappings = $this->getMappingsFromProviderData();
        $this->setMappings($mappings);
        $this->setProcessorVariablesFromMapping();
    }

    /**
     * @throws MappingException
     * @throws MappingBucketException
     * @throws Exception
     * @throws ExceptionInterface
     */
    protected abstract function setProcessorVariablesFromMapping(): void;
}