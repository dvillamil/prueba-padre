<?php

namespace App\Processor\Provider\Rushbet\Json;

use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\Register;
use AsResultados\OAMBundle\Api\Internal\Bets\Match\Register as RegisterBet;
use AsResultados\OAMBundle\Api\Internal\Bets\Match\Request as RequestBet;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Request;
use AsResultados\OAMBundle\Api\Internal\Editor\CompetitionActiveData\Request as RequestCompetitionActiveData;
use AsResultados\OAMBundle\Api\Internal\Editor\ConfigBet\Request as RequestConfigBet;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\Request as MappingRequest;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\Resource as MappingResource;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Bets\Match\Embed\Due;
use AsResultados\OAMBundle\Model\Bets\Match\Match;
use App\Utils\Date;
use Exception;
use Psr\Http\Message\RequestInterface;

class Bet extends AbstractProcessor
{
    /**
     * @var array
    */
    private $matchProviderId = array();

    /**
     * @var array
    */
    private $matchesId = array();

     /**
     * @var array
    */
    private $matchesIdRegistered = array();

    /**
     * @var array
    */
    private $betOffers = array();

     /**
     * @var array
    */
    private $betOffersRegistered = array();

    /**
     * @var array
    */
    private $competitionsIds = array();

    /**
     * @var array
    */
    private $matchesCompetitionsIds = array();

    /**
     * @throws Exception
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $requests = array();
        $requestGenerator = new Request();
        $requests = $this->getRequestCompetitionSeason($requestGenerator);
        $this->getMatchesUnregistered($requests);
    }

    /**
     * @param Request $requestGenerator
     * @return array
     * @throws MappingException
     * @throws MissingItemException
     */

    private function getRequestCompetitionSeason(Request $requestGenerator): array
    {    
        $mappingCollection = MappingCollection::getInstance();
        $requests = array();

        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $competitions) {
                foreach($competitions as $key => $matches) {
                    try{       
                        $competitionId = $this->validateExistCompetition($mappingCollection, $key);
                        $competitionSeasonId = $this->getCompetitionSeasonId($competitionId, $mappingCollection);
                        $this->competitionsIds[$competitionSeasonId] = $competitionId;
                        $requests = array_merge($requests, $this->getRequestMatch($matches, $competitionSeasonId, $mappingCollection, $requestGenerator));
                       
                    }catch (MappingException $e) {
                        $this->getLogger()->error('The competition mapping not exist: ' . $e->getMessage());
                        continue;
                    }
                }
            }
        }

        return $requests;
    }

    /**
     * @param array $matches
     * @param string $competitionSeasonId
     * @param MappingCollection $mappingCollection
     * @param Request $requestGenerator
     * @return array
     * @throws MappingException
     * @throws MissingItemException
     */

    private function getRequestMatch(array $matches, string $competitionSeasonId, MappingCollection $mappingCollection, Request $requestGenerator): array
    {   
        $requests = array(); 

        foreach($matches as $match) {
            $date = $this->getDateTimeStamp($match['start']);
            
            try{     
                $matchId = $this->validateExistMatch($mappingCollection, $match);
                $this->matchesIdRegistered[$matchId] = $match['id']; //Id Mongo = Id Provider
                $this->betOffersRegistered[] = $this->transformBetsOffers($match['betOffers']);
            }catch (MissingItemException $e) {
                $this->getLogger()->error('The Property not exist: ' . $e->getMessage());
                continue;
            }catch (MappingException $e) {
                try{       
                    $requests[] = $this->getRequestTeam($match, $competitionSeasonId, $date, $mappingCollection, $requestGenerator);
                }catch (MissingItemException $e) {
                    $this->getLogger()->error('The Property not exist: ' . $e->getMessage());
                    continue;
                }catch (MappingException $e) {
                    $this->getLogger()->error('The team mapping not exist: ' . $e->getMessage());
                    continue;
                }
                continue;
               
            }
        }

        return $requests;
    }

    /**
     * @param array $match
     * @param string $competitionSeasonId
     * @param int $date
     * @param MappingCollection $mappingCollection
     * @param Request $requestGenerator
     * @return array
     * @throws MappingException
     * @throws MissingItemException
     */
    private function getRequestTeam(array $match, string $competitionSeasonId, int $date, MappingCollection $mappingCollection, Request $requestGenerator): RequestInterface
    {    
        $teams = $this->getTeamsFromJson($mappingCollection, $match);
        $this->matchProviderId[] = $match['id']; 
        $this->betOffers[] = $this->transformBetsOffers($match['betOffers']);
        $localId = $teams[0];
        $visitorId = $teams[1];
        return $requestGenerator->getIdByCompetitionSeasonLocalVisitorAndDate($competitionSeasonId, $localId, $visitorId, $date);
    }

    
    /**
     * @param array $betOffers
     * @return array
     */
    protected function transformBetsOffers(array $betOffers): array
    {
        $due = new Due();
        $item = array();

        foreach($betOffers as $outcomes){
            foreach($outcomes['outcomes'] as $key=>$value){
                if($key == 0){
                    $due->setLocal(floatval($value['odds']));
                }elseif($key == 1){
                    $due->setDraw(floatval($value['odds']));
                }elseif($key == 2){
                    $due->setVisitor(floatval($value['odds']));
                }  
                
            }
            $item[] = $due;
        }

        return $item;

    }

     /**
     * @param array $matchesUnregistered 
     * @param array $matchesRegisteredIdMongo
     * @return Collection
     */
    protected function createBetItemRegistered(array $matchesUnregistered, array $matchesRegisteredIdMongo): Collection
    {
        $result = new Collection(Match::class);
        $items = array();
        foreach($matchesUnregistered as $key=>$match){
            $items[] = $this->updatedMatchFromJsonNode($this->betOffersRegistered[$key], $matchesRegisteredIdMongo[$key]['_id']);
            $result->addRegistered($items[$key]);
        }
        return $result;
    }


    /**
     * @param array $requests 
     * @return array
     */
    protected function getMatchesUnregistered(array $requests): void
    {
        $betOffers =  array();
        foreach($requests as $key => $request){
            $response = $this->executeRequest($request);
            if(!empty($response['items'])){
                $this->matchesId[$response['items'][0]['_id']] = $this->matchProviderId[$key];
                $betOffers[] = $this->betOffers[$key];
                $this->matchesCompetitionsIds[$response['items'][0]['_id']] = $this->competitionsIds[$response['items'][0]['competition_season']];
            }else{
                $this->getLogger()->info('The match mapping not exists id: '. $this->matchProviderId[$key]);
            }
        }

        $this->betOffers = $betOffers;

    }

    /**
      *@param array $response 
     * @return array
     */
    protected function getMatchesUnregisteredDiarioAs(array $response): array
    {
        $providerId = array();
        $betOffers =  array();
        $MatchesIdDiarioAs = false;

        foreach($response['items'] as $key=>$match){
            $MatchesIdDiarioAs = false;
            foreach($match['providers'] as $provider){
                if($provider['provider'] == 'DIARIOAS'){
                    $providerId[] = $provider['id'];
                    $betOffers[] = $this->betOffers[$key];
                    $MatchesIdDiarioAs = true;
                }
            }
            if($MatchesIdDiarioAs == false){
                unset($this->matchesId[$match['_id']]); //Remove item dont have id Diarioas
                unset($this->matchesCompetitionsIds[$match['_id']]);
                $this->getLogger()->info('The provider mapping DiarioAs not exists: '.$match['_id']);
            }
        }
        $this->betOffers = $betOffers;
        
        return $providerId;

    }

     /**
      *@param array $response 
     * @return array
     */
    protected function getMatchesRegisteredDiarioAs(array $response): array
    {
        $providerId = array();
        $betOffers =  array();
        
        foreach($response['items'] as $key=>$match){
            foreach($match['providers'] as $provider){
                if($provider['provider'] == 'DIARIOAS'){
                    $providerId[] = $provider['id'];
                    $betOffers[] = $this->betOffersRegistered[$key];
                   
                }
            }
        }

        $this->betOffersRegistered = $betOffers;
        
        return $providerId;

    }
    
     /**
    * @param array $matchesUnregistered 
    * @param array $matchProvidersId 
    * @param array $matchesId 
     * @return Collection
     */
    protected function createBetItemUnregistered(array $matchesUnregistered, array $matchProvidersId, array  $matchesId): Collection
    {
        $result = new Collection(Match::class);
        $items = array();
        $editionsSlug = array();
        $competitionActive = null;
        foreach($matchesUnregistered as $key=>$match){
            if($competitionActive != $this->matchesCompetitionsIds[$matchesId[$key]]){ //This if make only one query for competition
                $editionsSlug = $this->getEditionActive($matchesId[$key]);
                $competitionActive = $this->matchesCompetitionsIds[$matchesId[$key]]; // Array pass the match and get the competition
            }
            $items[] = $this->createMatchFromJsonNode($match, $matchProvidersId[$key], $this->betOffers[$key], $editionsSlug);
            $result->addUnRegistered($items[$key], $match);
        }
        return $result;
    }

    /**
    *  @param RequestInterface $request 
     * @return array
     */
    protected function executeRequest(RequestInterface $request): array
    {
        $this->getClient()->addRequest($request);
        $this->getClient()->execute(true);
        return $this->getClient()->getResponseFor($request);
    }

   /**
    *  @param array $request 
     * @return array
     */
    protected function executeRequestMultiple(array $requests): array
    {
        $this->getClient()->addRequests($requests);
        $this->getClient()->execute(true);
        return $this->getClient()->getResponseForMultiple($requests);
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function run(): bool
    {

        $mappingRequest = new MappingRequest();
        $requestsMatches = array();
        $resource = new MappingResource();
        $register =  Register::getInstance($this->getClient());
        $registerBet = RegisterBet::getInstance($this->getClient());
        
        //Set Unregistered Matches Bet
        try {
            if(!empty($this->matchesId)){
                $requestsMatches = $mappingRequest->getByIds(array_keys($this->matchesId));
                $response = $this->executeRequestMultiple($requestsMatches);
                $matchesUnregistered = $this->getMatchesUnregisteredDiarioAs($response);//GET Matches WITH ID DIARIOAS
                if(!empty($matchesUnregistered))
                {  
                    $matchesId =  array_keys($this->matchesId);
                    $matchesProvidersIds = array_values($this->matchesId);
                    $mappings = $resource->createMappingsPatchItemFromModel($matchesId, $matchesProvidersIds, self::PROVIDER);
                    $register->patch($mappings);//path the matchesId not matches with the mappings
                    
                    $result = $this->createBetItemUnregistered($matchesUnregistered, $matchesProvidersIds, $matchesId);
                    $registerBet->post($result->getAllUnRegistered());
                }
            }
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not post bet matches: ' . $e->getMessage());
        }

         //Patch Registered Matches Bet
         try {
            if(!empty($this->matchesIdRegistered)){
                $requestsMatchesRegistered = $mappingRequest->getByIds(array_keys($this->matchesIdRegistered));
                $response = $this->executeRequestMultiple($requestsMatchesRegistered);
                $matchesRegistered = $this->getMatchesRegisteredDiarioAs($response);
                if(!empty($matchesRegistered)){
                    $matchesRegisteredIdMongo = $this->getMatchesRegisteredIdMongo($matchesRegistered);
                    $result = $this->createBetItemRegistered($matchesRegistered, $matchesRegisteredIdMongo);
                    $registerBet->patch($result->getAllRegistered());
                }
            }
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not patch bet matches: ' . $e->getMessage());
        }
        
        return true;
    }

    /**
     * @param array $matchesRegistered 
     * @return array['items']
     * 
     */
    protected function getMatchesRegisteredIdMongo(array $matchesRegistered): array
    {
        $requestBet = new RequestBet();
        $requestsIds = $requestBet->getByEvents($matchesRegistered, self::PROVIDER);
        $response = $this->executeRequestMultiple($requestsIds);
        return $response['items'];
    }


    /**
     * @param MappingCollection $mappingCollection 
     * @param array $match 
     * @return array
     * @throws Exception
     */
    protected function getTeamsFromJson(MappingCollection $mappingCollection, array $match): array
    {
        $teams = [];
        foreach($match['participants'] as $team) {
            if (isset($team['id'])){
                $teams[] = $mappingCollection->get($mappingCollection::ENTITY_TEAM, self::PROVIDER, $team['id']);
            }else{
                 throw new MissingItemException(
                    "team['id']",
                    $mappingCollection::ENTITY_TEAM,
                    "No exist"
                );
            } 
        }    
        
        return $teams;
       
    }

    /**
     * @param string $match 
     * @return array
    */
    protected function getEditionActive(string $match): array
    {
        $editions = array();
        $RequestConfigBet = new RequestConfigBet();
        $competitionId = $this->matchesCompetitionsIds[$match];
        $request = $RequestConfigBet->getByActiveProvider(self::PROVIDER, $competitionId);
        $response = $this->executeRequest($request);
        foreach($response['items'] as $edition) {  
            $editions[] = $edition['edition']['slug']; //Get The editions
        }
       
        return $editions;
    }

    /**
     * @param string $date 
     * @return int
    */
    protected function getDateTimeStamp(string $date): int
    {
        $fecha = new date($date);
        return $fecha->getTimestamp();
    }

    /**
     * @param MappingCollection $mappingCollection 
     * @param string $CompetitionId 
     * @throws MappingException
     * @return string
    */
    protected function getCompetitionSeasonId(string $CompetitionId, MappingCollection $mappingCollection): string
    {
        $RequestCompetitionActiveData = new RequestCompetitionActiveData();
        $request = $RequestCompetitionActiveData->getByCompetitionCurrent($CompetitionId);
        $response = $this->executeRequest($request);
        $firstCompetitionSeason = reset($response['items']); //Get the first element of array 
        if(isset($firstCompetitionSeason['competition_season']) && $firstCompetitionSeason['competition_season']){
            return $firstCompetitionSeason['competition_season'];
        }else{
            throw new MappingException(
                $mappingCollection::ENTITY_COMPETITION,
                self::PROVIDER,
                $CompetitionId
            );
        } 
    }

    /**
     * @param MappingCollection $mappingCollection 
     * @param string $key
     * @throws MappingException
     * @return string
    */
    private function validateExistCompetition(MappingCollection $mappingCollection, string $key): string{
        return $mappingCollection->get($mappingCollection::ENTITY_COMPETITION, self::PROVIDER, $key);
    }


    /**
     * @param MappingCollection $mappingCollection 
     * @param array $match
     * @throws MappingException
     * @throws MissingItemException
     * @return string
    */
    private function validateExistMatch(MappingCollection $mappingCollection, array $match): string{
        if (isset($match['id'])){
            return $mappingCollection->get($mappingCollection::ENTITY_MATCH, self::PROVIDER, $match['id']);
        }else{
             throw new MissingItemException(
                "match['id']",
                $mappingCollection::ENTITY_MATCH,
                "No exist"
            );
        }
        
    }

    /**
     * @param string $match
     * @param string $url
     * @param array $betOffers
     * @param array $editionsSlug
     * @return Match
     * 
     */
    protected function createMatchFromJsonNode(string $match, string $url, array $betOffers, array $editionsSlug): Match
    {
        $item = new Match();
        $item->setEvent($match);
        $item->setEditions($editionsSlug);
        $baseUrl = $item->getBaseUrl('/'.self::RUSHBET_EVENT);
        $url = $baseUrl.'/'.$url;
        $item->setUrl($url);
        $item->setProvider(self::PROVIDER);
        $item->setDues($betOffers);
        return $item;
    }

    /**
     * 
     * @param array $betOffers
     * @param string $id
     * @return Match
     * 
     */
    protected function updatedMatchFromJsonNode(array $betOffers, string $id): Match
    {
        $item = new Match();
        $item->setId($id);
        $item->setDues($betOffers);
        return $item;
    }

    /**
     * @return ProviderIdsCollection[]
     * @throws Exception
     */
    protected function getMappingsFromProviderData(): array
    {
        return $this->getMappingAllFromJson();
    }

     /**
     * @return array(ProviderIdsCollection)
     * @throws MissingItemException
     */
    protected function getMappingAllFromJson(): array
    {
        $collectionCompetition = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION, self::PROVIDER);
        $collectionMatch = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH, self::PROVIDER);
        $collectionTeam = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        
        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $competitions) {
                foreach($competitions as $key => $matches) {
                    $collectionCompetition->addId($key);
                    foreach($matches as $match) {
                        if (isset($match['id'])){
                            $collectionMatch->addId($match['id']);
                        }
                        if (isset($match['participants'])){
                            foreach($match['participants'] as $teams) {
                                if (isset($teams['id'])){
                                    $collectionTeam->addId($teams['id']);
                                }
                            }     
                        }
                    }
                }
            }
        }
        return [$collectionCompetition, $collectionMatch, $collectionTeam];
    }

}