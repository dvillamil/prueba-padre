<?php

namespace App\Processor\Provider\Besoccer\Json;

use App\Event\ResourceProcessed\Roster\RosterTeamsEvent;
use App\Processor\Provider\Traits\CompetitionSeasonsTrait;
use App\Utils\StringTrait;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Category\CategoryInterface;
use AsResultados\OAMBundle\Api\Internal\Results\RequestsResourceItem;
use AsResultados\OAMBundle\Api\Internal\Results\Team\Register as TeamRegister;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Register as TeamSeasonRegister;
use AsResultados\OAMBundle\Api\Internal\Results\TeamCompetitionSeason\Request as TeamCompetitionSeasonRequest;
use AsResultados\OAMBundle\Exception\DatabaseInconsistencyException;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingBucketException;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\CompetitionSeason\CompetitionSeason;
use AsResultados\OAMBundle\Model\Results\Person\Person;
use AsResultados\OAMBundle\Model\Results\Team\Embed\Name as TeamName;
use AsResultados\OAMBundle\Model\Results\Team\Team;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\Embed\Name as TeamCompetitionSeasonName;
use AsResultados\OAMBundle\Model\Results\TeamCompetitionSeason\TeamCompetitionSeason;
use Exception;

class Players extends AbstractProcessor
{
    use CompetitionSeasonsTrait;
    use StringTrait;

    /**
     * @throws Exception
     */
    protected function setProcessorVariablesFromMapping(): void
    {
        $mappingCollection = MappingCollection::getInstance();
        $competitionSeasons = array_values($mappingCollection->getMultiple(
            MappingInterface::ENTITY_COMPETITION_SEASON,
            self::PROVIDER
        ));
        if (empty($competitionSeasons)) {
            $exceptions = array();
            foreach ($this->getMappingCompetitionSeasonsFromJson()->getIds() as $id) {
                $exceptions[] = new MappingException(
                    self::PROVIDER,
                    $id,
                    MappingInterface::ENTITY_COMPETITION_SEASON
                );
            }
            throw new MappingBucketException($exceptions);
        }
        $this->setCompetitionSeasons($competitionSeasons, $this->getClient());
        $this->setMappingsOwn($this->getMappingsOwn());
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function run(): bool
    {
        //Get teams
        $teams = $this->getTeamsFromJson();
        $teamRegister = TeamRegister::getInstance($this->getClient());
        //Insert teams
        try {
            $teamRegister->postWithMapping(
                $teams->getAllUnRegistered(), $teams->getUnRegisteredIds(), MappingInterface::ENTITY_TEAM, self::PROVIDER
            );
            $opInsertTeams = true;
            //Add new teams to collection
            $teams->removeAllUnRegistered();
            $teams->addMultipleRegistered($teamRegister->getLastInsertedItems());
        } catch (DatabaseInconsistencyException $e) {
            $this->getLogger()->emergency('Can not insert teams: ' . $e->getMessage());
            throw $e;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert teams: ' . $e->getMessage());
        }
        //Get teamsCompetitionSeason
        $teamsCompetitionSeason = $this->getTeamsCompetitionsSeasonsFromJson();
        $teamSeasonRegister = TeamSeasonRegister::getInstance($this->getClient());
        //Update teamsCompetitionSeason
        try {
            $teamSeasonRegister->patch($teamsCompetitionSeason->getAllRegistered());
            $opUpdateTeamsCompetitionSeason = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update teamsCompetitionSeason: ' . $e->getMessage());
        }
        //Insert teamsCompetitionSeason
        try {
            $teamSeasonRegister->post($teamsCompetitionSeason->getAllUnRegistered());
            $opInsertTeamsCompetitionSeason = true;
            //Add new teamsCompetitionSeason to collection
            $teamsCompetitionSeason->removeAllUnRegistered();
            $teamsCompetitionSeason->addMultipleRegistered($teamSeasonRegister->getLastInsertedItems());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not insert teamsCompetitionSeason: ' . $e->getMessage());
        }
        //Update teamsCompetitionSeason inside teams
        $teamsCompetitionSeason->eachRegistered(function (TeamCompetitionSeason $teamCompetitionSeason) use (&$teams) {
            if ($teams->existsRegistered($teamCompetitionSeason->getTeam()->getId())) {
                $tmp = $teams->getRegistered($teamCompetitionSeason->getTeam()->getId());
                if ($tmp instanceof Team) {
                    $competitionsSeasons = $tmp->getCompetitionsSeasons();
                    if (!is_array($competitionsSeasons)) {
                        $competitionsSeasons = array();
                    }
                    $competitionsSeasons[] = $teamCompetitionSeason->getId();
                    $tmp->setCompetitionsSeasons($competitionsSeasons);
                }
            }
        });
        //Update teams
        try {
            $teamRegister->patch($teams->getAllRegistered());
            $opUpdateTeams = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update teams: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opInsertTeams) || isset($opInsertTeamsCompetitionSeason) ||
            isset($opUpdateTeams) || isset($opUpdateTeamsCompetitionSeason)) {
            $this->dispatchEvents();
        }
        if (isset($opInsertTeams) || isset($opInsertTeamsCompetitionSeason)) {
            $this->dispatchEventsInsertTeams();
        }
        return true;
    }

    private function dispatchEvents(): void
    {
    }

    private function dispatchEventsInsertTeams(): void
    {
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            $event = new RosterTeamsEvent();
            $event->setCompetitionSeason($competitionSeason->getId());
            $this->dispatcher->dispatch($event, $event->getEventName());
        }
    }

    /**
     * @throws Exception
     */
    protected function getMappingsOwn(): array
    {
        $requests = array();
        $mappingCollection = MappingCollection::getInstance();
        $requestTeam = new TeamCompetitionSeasonRequest();
        foreach ($this->getCompetitionSeasons() as $competitionSeason) {
            $requests[] = new RequestsResourceItem(
                $requestTeam->getByTeamsAndCompetitionSeason(
                    $mappingCollection->getMultiple(MappingInterface::ENTITY_TEAM, self::PROVIDER),
                    $competitionSeason->getId()
                ),
                $requestTeam->getResource()
            );
        }
        return $requests;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getTeamsFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(Team::class);
        if (isset($this->getJsonDocument()['data']) && is_array($this->getJsonDocument()['data'])) {
            foreach ($this->getJsonDocument()['data'] as $node) {
                try {
                    $id = $node['id'];
                    if (empty($id)) {
                        //No id, skip it
                        continue;
                    }
                    $item = $this->createTeamFromJsonNode($node);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_TEAM, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_TEAM, self::PROVIDER, $id));
                        //Set slug to null because it should be already inserted
                        $item->setSlug(null);
                        //Set competition data
                        $competitionSeason = $this->getTeamCompetitionSeasons(
                            $mappingCollection->get(
                                $mappingCollection::ENTITY_TEAM,
                                self::PROVIDER,
                                $id
                            )
                        );
                        if (is_array($competitionSeason)) {
                            $competitionSeason = $this->getCompetitionSeason(reset($competitionSeason));
                            if ($competitionSeason->getCompetition()->getType()->getId() === CategoryInterface::COMPETITION_TYPE_NATIONAL_TEAM) {
                                $item->setNationalTeam(true);
                            } else {
                                $item->setNationalTeam(false);
                            }
                            $item->setGender($competitionSeason->getCompetition()->getGender());
                        }
                    }
                    if ($mappingCollection->exists($mappingCollection::ENTITY_TEAM, self::PROVIDER, $id)) {
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    //Error, skip item and keep going
                }
            }
        }
        return $result;
    }

    /**
     * @param array $node
     * @return Team
     */
    protected function createPlayerFromJsonNode(array $node): Team
    {
        $team = new Person();
        $team->setYearFoundation($node['info']['year_foundation'] ?? null);
        $name = new TeamName();
        $name->setOfficial($node['name']['official']);
        $name->setKnown($node['name']['know']);
        $name->setAbbreviation($node['name']['abbreviation']);
        $team->setName($name);
        if (!is_null($name->getOfficial())) {
            $team->setSlug($this->normalizeString($name->getOfficial()));
        } else {
            $team->setSlug($this->normalizeString($name->getKnown()));
        }
        return $team;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getTeamsCompetitionsSeasonsFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(TeamCompetitionSeason::class);
        if (isset($this->getJsonDocument()['data']) && is_array($this->getJsonDocument()['data'])) {
            foreach ($this->getJsonDocument()['data'] as $node) {
                try {
                    $itemTmp = $this->createTeamsCompetitionsSeasonsFromJsonNode($node);
                    $competitionSeasons = $this->getTeamCompetitionSeasons($itemTmp->getTeam()->getId());
                    foreach ($competitionSeasons as $competitionSeason) {
                        $item = clone($itemTmp);
                        $competitionSeasonObject = new CompetitionSeason();
                        $competitionSeasonObject->setId($competitionSeason);
                        $item->setCompetitionSeason($competitionSeasonObject);
                        if ($mappingCollection->existsOwn(TeamCompetitionSeason::class, $item->getUniqueIdByRelations())) {
                            $item->setId($mappingCollection->getOwn(TeamCompetitionSeason::class, $item->getUniqueIdByRelations()));
                            $result->addRegistered($item);
                        } else {
                            $result->addUnRegistered($item, $item->getUniqueIdByRelations());
                        }
                    }
                } catch (Exception $e) {
                    //Error error, skip item and keep going
                }
            }
        }
        return $result;
    }

    /**
     * @param array $node
     * @return TeamCompetitionSeason
     * @throws MappingException
     */
    protected function createTeamsCompetitionsSeasonsFromJsonNode(array $node): TeamCompetitionSeason
    {
        $mappingCollection = MappingCollection::getInstance();
        $teamCompetitionSeason = new TeamCompetitionSeason();
        $teamCompetitionSeason->setTeamById($mappingCollection->get(
            $mappingCollection::ENTITY_TEAM,
            self::PROVIDER,
            $node['id']
        ));
        $name = new TeamCompetitionSeasonName();
        $name->setKnown($node['name']['know']);
        $teamCompetitionSeason->setName($name);
        return $teamCompetitionSeason;
    }

    /**
     * @return ProviderIdsCollection[]
     * @throws Exception
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingCompetitionSeasonsFromJson();
        $mappings[] = $this->getMappingPlayersFromJson();
        return $mappings;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPlayersFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        if (isset($this->getJsonDocument()['data']) && is_array($this->getJsonDocument()['data'])) {
            foreach ($this->getJsonDocument()['data'] as $player) {
                $collection->addId($player['id']);
            }
        }
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingCompetitionSeasonsFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_COMPETITION_SEASON, self::PROVIDER);
        if (isset($this->getJsonDocument()['data']) && is_array($this->getJsonDocument()['data'])) {
            foreach ($this->getJsonDocument()['data'] as $player) {
                if (isset($team['trajectory']) && is_array($player['trajectory'])) {
                    foreach ($player['trajectory'] as $competitions_season) {
                        try {
                            $competitionSeasonId = $this->generateProviderCompetitionSeasonId(
                                $competitions_season['id'],
                                $competitions_season['season']
                            );
                            $collection->addId($competitionSeasonId);
                        } catch (Exception $e) {
                            //Keep going, we do not use that one but we work with the rest of them
                        }
                    }
                }
            }
        }
        return $collection;
    }

    /**
     * @param string $id
     * @return string[]
     * @throws MissingItemException
     * @throws MappingException
     */
    protected function getTeamCompetitionSeasons(string $id): array
    {
        if (!isset($this->teamsInCompetitionSeasons)) {
            $mappingCollection = MappingCollection::getInstance();
            $result = array();
            if (isset($this->getJsonDocument()['data']) && is_array($this->getJsonDocument()['data'])) {
                foreach ($this->getJsonDocument()['data'] as $team) {
                    if (!$mappingCollection->exists($mappingCollection::ENTITY_TEAM, self::PROVIDER, $team['id'])) {
                        //We do not have mapping for that team
                        continue;
                    }
                    $id = $mappingCollection->get($mappingCollection::ENTITY_TEAM, self::PROVIDER, $team['id']);
                    if (isset($team['competitions_seasons']) && is_array($team['competitions_seasons'])) {
                        foreach ($team['competitions_seasons'] as $competitions_season) {
                            try {
                                $competitionSeasonId = $this->generateProviderCompetitionSeasonId(
                                    $competitions_season['competition'],
                                    $competitions_season['season']
                                );
                                if (!$mappingCollection->exists($mappingCollection::ENTITY_COMPETITION_SEASON, self::PROVIDER, $competitionSeasonId)) {
                                    //We do not have mapping for that team
                                    continue;
                                }
                                $competitionSeasonId = $mappingCollection->get($mappingCollection::ENTITY_COMPETITION_SEASON, self::PROVIDER, $competitionSeasonId);
                                if (!isset($result[$id][$competitionSeasonId])) {
                                    $result[$id][$competitionSeasonId] = $competitionSeasonId;
                                }
                            } catch (Exception $e) {
                                //Keep going, we do not use that competition season but we work with the rest of them
                                continue;
                            }
                        }
                    }
                }
            }
            $this->teamsInCompetitionSeasons = $result;
        }
        if (!isset($this->teamsInCompetitionSeasons[$id])) {
            throw new MissingItemException($id, 'teamsInCompetitionSeason', 'can not associate team id to any competitionSeason');
        }
        return $this->teamsInCompetitionSeasons[$id];
    }
}