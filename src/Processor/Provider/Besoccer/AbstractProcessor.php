<?php

namespace App\Processor\Provider\Besoccer;

use App\Processor\Provider\AbstractProcessor as AbstractProcessorProvider;
use Exception;

abstract class AbstractProcessor extends AbstractProcessorProvider implements ProcessorInterface
{
    /**
     * @param string $competitionId
     * @param string $seasonId
     * @return string
     * @throws Exception
     */
    protected function generateProviderCompetitionSeasonId(string $competitionId, string $seasonId): string
    {
        if (empty($competitionId)) {
            throw new Exception('Competition id is empty');
        }
        if (empty($seasonId)) {
            throw new Exception('Season id is empty');
        }
        return $competitionId . '_' . $seasonId;
    }
}