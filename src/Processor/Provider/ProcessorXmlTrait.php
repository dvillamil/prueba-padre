<?php

namespace App\Processor\Provider;

use Gaufrette\Filesystem;
use Symfony\Component\DomCrawler\Crawler;

trait ProcessorXmlTrait
{
    /**
     * @var Crawler
     */
    private $crawler;

    protected abstract function getFilePath(): string;

    protected abstract function getFileSystem(): Filesystem;

    /**
     * @param string|null $filePath
     * @return Crawler
     */
    protected function getCrawledXmlDocument(?string $filePath = null): Crawler
    {
        if (is_null($filePath)){
            $filePath = $this->getFilePath();
        }
        if (!isset($this->crawler[$filePath])) {
            $crawler = new Crawler();
            $xml_content = $this->getFileSystem()->read($filePath);
            $crawler->addXmlContent($xml_content);
            $this->crawler[$filePath] = $crawler;
        }
        return $this->crawler[$filePath];
    }
}