<?php

namespace App\Processor\Provider;

use Gaufrette\Filesystem;

trait ProcessorJsonTrait
{
    /**
     * @var array[]
     */
    private $json;

    protected abstract function getFilePath(): string;

    protected abstract function getFileSystem(): Filesystem;

    /**
     * @return array
     */
    protected function getJsonDocument(): array
    {
        if (!isset($this->json[$this->getFilePath()])) {
            $json_content = $this->getFileSystem()->read($this->getFilePath());
            $this->json[$this->getFilePath()] = json_decode($json_content, true);
        }
        return $this->json[$this->getFilePath()];
    }
}