<?php

namespace App\Processor\Provider;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;

Interface EventDispatcherDependencyInterface
{
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher);
}