<?php

namespace App\Processor\Provider\Diarioas\Json;

use App\Event\ResourceProcessed\Live\LiveMatchEvent;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Match\Register;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Exception\ValidationItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\Match\Embed\News;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Url;
use AsResultados\OAMBundle\Model\Results\Match\Match as MatchMaster;
use Exception;

class Match extends AbstractProcessor
{
    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        $register = Register::getInstance($this->getClient());
        //Get matches
        $matches = $this->getMatchesFromJson();
        try {
            $register->patch($matches->getAllRegistered());
            $opUpdateMatch = true;
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update matches: ' . $e->getMessage());
        }
        //Dispatch events
        if (isset($opUpdateMatch)) {
            $this->dispatchEvents($matches->getAllRegistered());
        }
        return true;
    }

    /**
     * @param MatchMaster[] $matches
     */
    private function dispatchEvents(array $matches): void
    {
        foreach ($matches as $match) {
            $event = new LiveMatchEvent();
            $event->setMatch($match->getId());
            $this->dispatcher->dispatch($event, $event->getEventName());
        }
    }

    /**
     * @return Collection
     */
    protected function getMatchesFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(MatchMaster::class);
        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $match) {
                try {
                    if (!isset($match['id'])) {
                        throw new ValidationItemException('id missing', 'id required', 'node match');
                    }
                    $id = $match['id'];
                    $item = $this->createMatchFromJson($match);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_MATCH, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_MATCH, self::PROVIDER, $id));
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    $this->getLogger()->warning('Can not create match: ' . $e->getMessage());
                    continue;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $node
     * @return MatchMaster
     * @throws MappingException
     * @throws MissingItemException
     */
    protected function createMatchFromJson(array $node): MatchMaster
    {
        $mappingCollection = MappingCollection::getInstance();
        $match = new MatchMaster();
        $match->setId($node['id']);
        $url = new Url();
        if (isset($node['url'])) {
            $url->setMatch($node['url']);
        }
        $allUrlNews = [];
        if (isset($node['edition']) && is_array($node['edition'])) {
            foreach ($node['edition'] as $edition) {
                $urlNews = new News();
                $editionId = $mappingCollection->get($mappingCollection::ENTITY_EDITION, self::PROVIDER, $edition['slug']);
                $urlNews->setEdition($editionId);
                if (isset($edition['preMatch'])) {
                    $urlNews->setPreMatch($edition['preMatch']);
                }
                if (isset($edition['live'])) {
                    $urlNews->setLive($edition['live']);
                }
                if (isset($edition['postMatch'])) {
                    $urlNews->setFinished($edition['postMatch']);
                }
                $allUrlNews[] = $urlNews;
            }
        }
        $url->setNews($allUrlNews);
        $match->setUrl($url);
        return $match;
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingMatchesFromJson();
        $mappings[] = new ProviderIdsCollection(MappingInterface::ENTITY_EDITION, self::PROVIDER, true);
        return $mappings;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingMatchesFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH, self::PROVIDER);
        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $match) {
                $collection->addId($match['id']);
            }
        }
        return $collection;
    }
}