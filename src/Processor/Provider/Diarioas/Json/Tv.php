<?php

namespace App\Processor\Provider\Diarioas\Json;

use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Tv\Register;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\ValidationItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\Tv\Tv as TvMaster;
use Exception;

class Tv extends AbstractProcessor
{
    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        $register = Register::getInstance($this->getClient());
        //Get tvs
        $tvs = $this->getTvsFromJson();
        try {
            $register->patch($tvs->getAllRegistered());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update tvs: ' . $e->getMessage());
        }
        return true;
    }

    /**
     * @return Collection
     */
    protected function getTvsFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(TvMaster::class);
        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $tv) {
                try {
                    if (!isset($tv['id'])) {
                        throw new ValidationItemException('id missing', 'id required', 'node tv');
                    }
                    $id = $tv['id'];
                    $item = $this->createTvFromJson($tv);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_TV, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_TV, self::PROVIDER, $id));
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    $this->getLogger()->warning('Can not create tv: ' . $e->getMessage());
                    continue;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $node
     * @return TvMaster
     */
    protected function createTvFromJson(array $node): TvMaster
    {
        $tv = new TvMaster();
        $tv->setImageId($node['id']);
        if (isset($node['nombre_normalizado'])) {
            $tv->setSlug($node['nombre_normalizado']);
        }
        return $tv;
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingTvsFromJson();
        return $mappings;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTvsFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TV, self::PROVIDER);
        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $tv) {
                $collection->addId($tv['id']);
            }
        }
        return $collection;
    }
}