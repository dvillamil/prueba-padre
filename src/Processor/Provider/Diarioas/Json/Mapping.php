<?php

namespace App\Processor\Provider\Diarioas\Json;

use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\Register;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\MappingException;
use AsResultados\OAMBundle\Exception\MissingItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Provider\EntityType\EntityType;
use AsResultados\OAMBundle\Model\Provider\Mapping\Embed\Provider;
use AsResultados\OAMBundle\Model\Provider\Mapping\Mapping as MappingModel;
use AsResultados\OAMBundle\Model\Provider\Provider\Provider as ProviderMaster;
use Exception;

class Mapping extends AbstractProcessor
{
    /**
     * @var string
     */
    private $provider;

    /**
     * @throws Exception
     */
    protected function setProcessorVariablesFromMapping(): void
    {
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function run(): bool
    {
        $register = Register::getInstance($this->getClient());
        //Get matches
        $matches = $this->getMatchesFromJson();
        try {
            $register->patch($matches->getAllRegistered());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update matches: ' . $e->getMessage());
        }
        //Get teams
        $teams = $this->getTeamsFromJson();
        try {
            $register->patch($teams->getAllRegistered());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update teams: ' . $e->getMessage());
        }
        //Get people
        $people = $this->getPeopleFromJson();
        try {
            $register->patch($people->getAllRegistered());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update people: ' . $e->getMessage());
        }
        //Get tvs
        $tvs = $this->getTvsFromJson();
        try {
            $register->patch($tvs->getAllRegistered());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update tvs: ' . $e->getMessage());
        }
        return true;
    }


    /**
     * @return Collection
     * @throws Exception
     */
    protected function getMatchesFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(MappingModel::class);
        if (isset($this->getJsonDocument()['match']) && is_array($this->getJsonDocument()['match'])) {
            foreach ($this->getJsonDocument()['match'] as $match) {
                try {
                    $item = $this->createMappingFromJsonNode($match, $mappingCollection::ENTITY_MATCH);
                    $result->addRegistered($item);
                } catch (MappingException $e) {
                    //We do not have that item, skip it but continue;
                    continue;
                }
            }
        }
        return $result;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getTeamsFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(MappingModel::class);
        if (isset($this->getJsonDocument()['team']) && is_array($this->getJsonDocument()['team'])) {
            foreach ($this->getJsonDocument()['team'] as $team) {
                try {
                    $item = $this->createMappingFromJsonNode($team, $mappingCollection::ENTITY_TEAM);
                    $result->addRegistered($item);
                } catch (MappingException $e) {
                    //We do not have that item, skip it but continue;
                    continue;
                }
            }
        }
        return $result;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getPeopleFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(MappingModel::class);
        if (isset($this->getJsonDocument()['person']) && is_array($this->getJsonDocument()['person'])) {
            foreach ($this->getJsonDocument()['person'] as $person) {
                try {
                    $item = $this->createMappingFromJsonNode($person, $mappingCollection::ENTITY_PERSON);
                    $result->addRegistered($item);
                } catch (MappingException $e) {
                    //We do not have that item, skip it but continue;
                    continue;
                }
            }
        }
        return $result;
    }

    /**
     * @return Collection
     * @throws Exception
     */
    protected function getTvsFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(MappingModel::class);
        if (isset($this->getJsonDocument()['tv']) && is_array($this->getJsonDocument()['tv'])) {
            foreach ($this->getJsonDocument()['tv'] as $tv) {
                try {
                    $item = $this->createMappingFromJsonNode($tv, $mappingCollection::ENTITY_TV);
                    $result->addRegistered($item);
                } catch (MappingException $e) {
                    //We do not have that item, skip it but continue;
                    continue;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $mapping
     * @param string $type
     * @return MappingModel
     * @throws MappingException
     * @throws MissingItemException
     */
    protected function createMappingFromJsonNode(array $mapping, string $type): MappingModel
    {
        $mappingCollection = MappingCollection::getInstance();
        $item = new MappingModel();
        $item->setId($mappingCollection->get($type, $this->getProviderFromJson(), $mapping['provider_id']));
        $entityType = new EntityType();
        $entityType->setId($type);
        $item->setType($entityType);
        $mappingProvider = new Provider();
        $providerMaster = new ProviderMaster();
        $providerMaster->setId(self::PROVIDER);
        $mappingProvider->setProvider($providerMaster);
        $mappingProvider->setId($mapping['id']);
        $item->setProviders([$mappingProvider]);
        return $item;
    }

    /**
     * @return ProviderIdsCollection[]
     * @throws Exception
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingMatchesFromJson();
        $mappings[] = $this->getMappingTeamsFromJson();
        $mappings[] = $this->getMappingPeopleFromJson();
        $mappings[] = $this->getMappingTvsFromJson();
        return $mappings;
    }

    /**
     * @return ProviderIdsCollection
     * @throws MissingItemException
     */
    protected function getMappingMatchesFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_MATCH, $this->getProviderFromJson());
        if (isset($this->getJsonDocument()['match']) && is_array($this->getJsonDocument()['match'])) {
            foreach ($this->getJsonDocument()['match'] as $match) {
                $collection->addId($match['provider_id']);
            }
        }
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     * @throws MissingItemException
     */
    protected function getMappingTeamsFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, $this->getProviderFromJson());
        if (isset($this->getJsonDocument()['team']) && is_array($this->getJsonDocument()['team'])) {
            foreach ($this->getJsonDocument()['team'] as $match) {
                $collection->addId($match['provider_id']);
            }
        }
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     * @throws MissingItemException
     */
    protected function getMappingPeopleFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, $this->getProviderFromJson());
        if (isset($this->getJsonDocument()['person']) && is_array($this->getJsonDocument()['person'])) {
            foreach ($this->getJsonDocument()['person'] as $match) {
                $collection->addId($match['provider_id']);
            }
        }
        return $collection;
    }

    /**
     * @return ProviderIdsCollection
     * @throws MissingItemException
     */
    protected function getMappingTvsFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TV, $this->getProviderFromJson());
        if (isset($this->getJsonDocument()['tv']) && is_array($this->getJsonDocument()['tv'])) {
            foreach ($this->getJsonDocument()['tv'] as $match) {
                $collection->addId($match['provider_id']);
            }
        }
        return $collection;
    }

    /**
     * @return string
     * @throws MissingItemException
     */
    protected function getProviderFromJson(): string
    {
        if (!isset($this->provider)) {
            if (!isset($this->getJsonDocument()['provider'])) {
                throw new MissingItemException('provider', 'string', 'json');
            }
            $this->provider = $this->getJsonDocument()['provider'];
        }
        return $this->provider;
    }
}