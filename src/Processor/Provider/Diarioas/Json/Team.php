<?php

namespace App\Processor\Provider\Diarioas\Json;

use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Team\Register;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\ValidationItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\Team\Embed\Internal;
use AsResultados\OAMBundle\Model\Results\Team\Embed\Name;
use AsResultados\OAMBundle\Model\Results\Team\Team as TeamMaster;
use Exception;

class Team extends AbstractProcessor
{
    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        $register = Register::getInstance($this->getClient());
        //Get teams
        $teams = $this->getTeamsFromJson();
        try {
            $register->patch($teams->getAllRegistered());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update teams: ' . $e->getMessage());
        }
        return true;
    }

    /**
     * @return Collection
     */
    protected function getTeamsFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(TeamMaster::class);
        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $team) {
                try {
                    if (!isset($team['id'])) {
                        throw new ValidationItemException('id missing', 'id required', 'node team');
                    }
                    $id = $team['id'];
                    $item = $this->createTeamFromJson($team);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_TEAM, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_TEAM, self::PROVIDER, $id));
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    $this->getLogger()->warning('Can not create team: ' . $e->getMessage());
                    continue;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $node
     * @return TeamMaster
     */
    protected function createTeamFromJson(array $node): TeamMaster
    {
        $team = new TeamMaster();
        $team->setId($node['id']);
        if (isset($node['tag'])) {
            $team->setTag($node['tag']);
        }
        if (isset($node['nombre_normalizado'])) {
            $team->setSlug($node['nombre_normalizado']);
        }
        if (isset($node['img'])) {
            $team->setImageId($node['img']);
        }
        if (isset($node['url'])) {
            $team->setUrl($node['url']);
        }
        $name = new Name();
        $internal = new Internal();
        if (isset($node['nombre_corto'])) {
            $internal->setKnown($node['nombre_corto']);
        }
        if (isset($node['abreviatura'])) {
            $internal->setAbbreviation($node['abreviatura']);
        }
        $name->setInternal($internal);
        $team->setName($name);
        return $team;
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingTeamsFromJson();
        return $mappings;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingTeamsFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_TEAM, self::PROVIDER);
        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $team) {
                $collection->addId($team['id']);
            }
        }
        return $collection;
    }
}