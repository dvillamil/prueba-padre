<?php

namespace App\Processor\Provider\Diarioas\Json;

use AsResultados\OAMBundle\Api\Internal\Provider\Mapping\MappingInterface;
use AsResultados\OAMBundle\Api\Internal\Results\Person\Register;
use AsResultados\OAMBundle\Exception\EmptyItemException;
use AsResultados\OAMBundle\Exception\ValidationItemException;
use AsResultados\OAMBundle\Model\Collection\Collection;
use AsResultados\OAMBundle\Model\Collection\MappingCollection;
use AsResultados\OAMBundle\Model\Collection\ProviderIdsCollection;
use AsResultados\OAMBundle\Model\Results\Person\Embed\Internal;
use AsResultados\OAMBundle\Model\Results\Person\Embed\Name;
use AsResultados\OAMBundle\Model\Results\Person\Person as PersonMaster;
use Exception;

class Person extends AbstractProcessor
{
    /**
     * @inheritDoc
     */
    protected function setProcessorVariablesFromMapping(): void
    {
    }

    /**
     * @inheritDoc
     */
    public function run(): bool
    {
        $register = Register::getInstance($this->getClient());
        //Get people
        $people = $this->getPeopleFromJson();
        try {
            $register->patch($people->getAllRegistered());
        } catch (EmptyItemException $e) {
        } catch (Exception $e) {
            $this->getLogger()->error('Can not update people: ' . $e->getMessage());
        }
        return true;
    }

    /**
     * @return Collection
     */
    protected function getPeopleFromJson(): Collection
    {
        $mappingCollection = MappingCollection::getInstance();
        $result = new Collection(PersonMaster::class);
        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $person) {
                try {
                    if (!isset($person['id'])) {
                        throw new ValidationItemException('id missing', 'id required', 'node person');
                    }
                    $id = $person['id'];
                    $item = $this->createPersonFromJson($person);
                    if ($mappingCollection->exists($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id)) {
                        $item->setId($mappingCollection->get($mappingCollection::ENTITY_PERSON, self::PROVIDER, $id));
                        $result->addRegistered($item);
                    } else {
                        $result->addUnRegistered($item, $id);
                    }
                } catch (Exception $e) {
                    $this->getLogger()->warning('Can not create person: ' . $e->getMessage());
                    continue;
                }
            }
        }
        return $result;
    }

    /**
     * @param array $node
     * @return PersonMaster
     */
    protected function createPersonFromJson(array $node): PersonMaster
    {
        $person = new PersonMaster();
        $person->setId($node['id']);
        if (isset($node['tag'])) {
            $person->setTag($node['tag']);
        }
        if (isset($node['nombre_normalizado'])) {
            $person->setSlug($node['nombre_normalizado']);
        }
        if (isset($node['img'])) {
            $person->setImageId($node['img']);
        }
        if (isset($node['url'])) {
            $person->setUrl($node['url']);
        }
        $name = new Name();
        $internal = new Internal();
        if (isset($node['nombre_corto'])) {
            $internal->setKnown($node['nombre_corto']);
        }
        $name->setInternal($internal);
        $person->setName($name);
        return $person;
    }

    /**
     * @inheritDoc
     */
    protected function getMappingsFromProviderData(): array
    {
        $mappings = array();
        $mappings[] = $this->getMappingPeopleFromJson();
        return $mappings;
    }

    /**
     * @return ProviderIdsCollection
     */
    protected function getMappingPeopleFromJson(): ProviderIdsCollection
    {
        $collection = new ProviderIdsCollection(MappingInterface::ENTITY_PERSON, self::PROVIDER);
        if (is_array($this->getJsonDocument())) {
            foreach ($this->getJsonDocument() as $person) {
                $collection->addId($person['id']);
            }
        }
        return $collection;
    }
}