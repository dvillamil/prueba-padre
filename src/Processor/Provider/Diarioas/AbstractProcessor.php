<?php

namespace App\Processor\Provider\Diarioas;

use App\Processor\Provider\AbstractProcessor as AbstractProcessorProvider;

abstract class AbstractProcessor extends AbstractProcessorProvider implements ProcessorInterface
{
}