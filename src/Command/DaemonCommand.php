<?php

namespace App\Command;

use Exception;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Uecode\Bundle\QPushBundle\Message\Message;
use Wrep\Daemonizable\Command\EndlessCommand;
use Uecode\Bundle\QPushBundle\Provider\ProviderInterface;
use Psr\Log\LoggerInterface;
use Knp\Bundle\GaufretteBundle\FilesystemMap;


class DaemonCommand extends EndlessCommand
{

    const TIME_TO_WAIT = 1;

    private $running_processes = [];
    private $messages = [];
    private $pending_shutdown = false;
    private $logger;
    private $queue;
    private $baseDir;
    private $filesystem;

    public function __construct(LoggerInterface $techLogger, ProviderInterface $queue, FilesystemMap $file_system_map)
    {
        $this->logger = $techLogger;
        $this->queue = $queue;
        $this->filesystem = $file_system_map->get(getenv('APP_FILE_SYSTEM'));
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('batch:daemon:run')
            ->setDescription('Initialize batch daemon')
            ->setTimeout(getenv('APP_DAEMON_TIMEOUT'));
    }

    protected function starting(InputInterface $input, OutputInterface $output)
    {
        $this->baseDir = $this->getApplication()->getKernel()->getProjectDir() . '/../';
        declare(ticks=1);
        pcntl_signal(SIGINT, [$this, 'handleSignal']);
        pcntl_signal(SIGTERM, [$this, 'handleSignal']);
        pcntl_signal(SIGHUP, [$this, 'handleSignal']);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->cleanFinishedProcesses();

        if (!$this->pending_shutdown) {
            $this->createProcessesOnDemand();
        }
    }

    private function createProcessesOnDemand()
    {
        $max_threads = getenv('APP_DAEMON_MAX_THREADS');
        $free_threads = $max_threads - count($this->running_processes);
        if ($free_threads > 0) {
            try {
                $this->messages = $this->queue->receive(['messages_to_receive' => $free_threads]);
            } catch (\Exception $e) {
                $this->logger->critical('Error receiving messages from queue with name: ' . $this->queue->getOptions()['queue_name']);
                throw $e;
            }

            if (!empty($this->messages)) {
                $filesBeingProcessed = array();
                $pending_messages = count($this->messages);
                $threads = $pending_messages >= $free_threads ? $free_threads : $pending_messages;
                for ($i = 0; $i < $threads; $i++) {
                    $message = $this->messages[$i];
                    $message_data = $message->getBody();
                    //If the file is being processed we delete this new message and skip it
                    if (in_array($message_data['file_storage_path'], $filesBeingProcessed)) {
                        $this->deleteQueueMessage($message, ' (Duplicate Item) ');
                        continue;
                    }

                    $command = ['php', 'bin/console', 'batch:process:run',
                        $message_data['file_storage_path'],
                        $message_data['feed_schema'],
                        $message_data['provider'],
                        $message_data['feed_type'],
                        $message->getId()
                    ];

                    if (isset($message_data['feature'])) {
                        array_push($command, '--feature', $message_data['feature']);
                    }

                    array_push($command, '--env', getenv('APP_ENV'));

                    /*
                     * $this->logger->info(join(' ', $command));
                     * $this->logger->close();
                    */
                    $process = new Process($command);
                    $process->enableOutput();
                    $process->start();
                    $this->running_processes[$process->getPid()] = array(
                        'message' => $message,
                        'process' => $process
                    );
                    $filesBeingProcessed[] = $message_data['file_storage_path'];
                }
            }
        }
    }

    private function cleanFinishedProcesses()
    {
        if (!empty($this->running_processes)) {
            foreach ($this->running_processes as $pid => $data) {
                $process = $data['process'];
                if (!$process->isRunning()) {
                    if (!empty($process->getOutput())) {
                        $this->logger->info($process->getOutput());
                    }
                    unset($this->running_processes[$pid]);
                    $this->deleteQueueMessage($data['message']);
                    if ($process->getExitCode() != 0) {
                        $message_data = $data['message']->getBody();
                        $this->logger->critical('Process [' . $message_data['feed_type'] . ' - ' . $message_data['file_storage_path'] . '] FAILED with message: ' . $process->getErrorOutput());
                    }
                }
            }
            $this->logger->close();
        } elseif ($this->pending_shutdown) {
            $this->logger->critical('Shutdown daemon (pending processes: ' . count($this->running_processes) . ')');
            exit(0);
        }
    }

    private function deleteQueueMessage(Message $message, $logText = '')
    {
        try {
            $this->queue->delete($message->getMetadata()->get('ReceiptHandle'));
        } catch (Exception $e) {
            $message_data = $message->getBody();
            $this->logger->warning('IMPOSSIBLE DELETE MESSAGE' . $logText . 'FOR FILE ' . $message_data['feed_type'] . '(' . $message_data['file_storage_path'] . ' ' . $message->getId() . ')');
            $this->logger->close();
        }
    }


    public function handleSignal($signal)
    {
        switch ($signal) {
            case SIGTERM:
            case SIGINT:
            case SIGHUP:
                $this->logger->critical('Received Signal (pending processes: ' . count($this->running_processes) . ')');
                $this->logger->close();
                $this->pending_shutdown = true;
                break;
        }
    }


}