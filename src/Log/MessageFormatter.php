<?php

namespace App\Log;

use Monolog\Formatter\FormatterInterface;

class MessageFormatter implements FormatterInterface
{
    const SERVICE = 'BATCH';
    const DAEMON = 'DAEMON';
    const PROCESSOR = 'PROCESSOR';

    private $time;

    public function format(array $record)
    {
        $this->time = (isset($record['context']['time'])) ? $record['context']['time'] : $record['datetime']->format('U.u');
        if (!isset($record['context']['initializeLog'])) {
            return sprintf("%-10s", $record['level_name']) . ' | ' . $this->time . ' | ' . $record['datetime']->format('Y.m.d H:i:s') . ' | ' . '[' . self::SERVICE . '] | ' . '[' . self::PROCESSOR . '] | ' . $this->getMessage($record) . "\n";
        } else {
            return $this->getMessage($record) . "\n";
        }
    }

    public function formatBatch(array $records)
    {
    }

    private function getMessage(array $record)
    {
        return $this->getMoment($record['context']) . $this->getElapsedSeconds($record['context']) . $this->getFunctionality($record['context']) . $record['message'];

    }

    private function getElapsedSeconds(array $context)
    {
        if (isset($context['startTime']) && !empty($context['startTime'])) {
            $startTime = floatval($this->time);
            $functionTime = floatval($context['startTime']);
            $totalTime = $startTime - $functionTime;
            return ' [ELAPSED TIME: ' . $totalTime . ' sec.] | ';
        }
        return '';
    }

    private function getFunctionality(array $context)
    {
        if (isset($context['functionality']) && !empty($context['functionality'])) {
            $functionality = $context['functionality'];
            return 'Function ' . ucfirst(($pos = strrpos($functionality, '\\')) ? substr($functionality, $pos + 1) : $functionality);
        }
        return '';
    }

    private function getMoment(array $context)
    {
        return (isset($context['moment'])) ? '[' . $context['moment'] . '] |' : '';
    }

}
