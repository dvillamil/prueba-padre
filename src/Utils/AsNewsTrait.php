<?php

namespace App\Utils;

use App\Api\External\AsNews\RequestInterface;
use App\Api\External\AsNews\ResourceInterface;
use AsResultados\OAMBundle\Api\StatusCodeInterface;
use AsResultados\OAMBundle\Client\ClientManager;
use App\Api\External\AsNews\Match\Model\Match as AsNewsMatch;
use App\Api\External\AsNews\Team\Model\Team as AsNewsTeam;
use App\Api\External\AsNews\Player\Model\Player as AsNewsPlayer;
use App\Api\External\AsNews\Tv\Model\Tv as AsNewsTv;
use AsResultados\OAMBundle\Model\Results\Match\Embed\News;
use AsResultados\OAMBundle\Model\Results\Match\Embed\Url;
use AsResultados\OAMBundle\Model\Results\Match\Match;
use AsResultados\OAMBundle\Model\Results\Person\Embed\Internal as PersonInternal;
use AsResultados\OAMBundle\Model\Results\Person\Embed\Name as PersonName;
use AsResultados\OAMBundle\Model\Results\Person\Person;
use AsResultados\OAMBundle\Model\Results\Team\Embed\Internal as TeamInternal;
use AsResultados\OAMBundle\Model\Results\Team\Embed\Name as TeamName;
use AsResultados\OAMBundle\Model\Results\Team\Team;
use App\Api\External\AsNews\ResourceInterface as AsNewsResourceInterface;
use AsResultados\OAMBundle\Model\Results\Tv\Tv;
use Exception;

trait AsNewsTrait
{
    private $maxProviderIds = 40;

    /**
     * @param string $provider
     * @param array $providerIds
     * @param RequestInterface $request
     * @param AsNewsResourceInterface $resource
     * @param ClientManager $client
     * @return AsNewsResourceInterface
     * @throws Exception
     */
    protected function getAsNewsAttributes(string $provider, array $providerIds, RequestInterface $request, ResourceInterface $resource, ClientManager $client): AsNewsResourceInterface
    {
        try {
            $requestGenerator = function (string $provider, array $providerIdsArray, RequestInterface $request) {
                $providerIds = '';
                foreach ($providerIdsArray as $index => $providerId) {
                    $providerIds .= filter_var($providerId, FILTER_SANITIZE_NUMBER_INT) . ',';
                }
                return $request->getByProviderIds($provider, substr($providerIds, 0, -1));
            };
            $requests = [];
            foreach (array_chunk($providerIds, $this->maxProviderIds, true) as $providerIdsArray) {
                $request_id = $requestGenerator($provider, $providerIdsArray, $request);
                $client->addRequest($request_id);
                $requests[] = $request_id;
            }
            $client->execute(true);
            foreach ($requests as $request) {

                $response = $client->getResponseFor($request, [StatusCodeInterface::STATUS_CODE_OK]);
                if (isset($response['items'])) {
                    foreach ($response['items'] as $asNewsAttributes) {
                        $resource->addResource($asNewsAttributes['entity_id'], $asNewsAttributes);
                    }
                }
            }
            return $resource;
        } catch (Exception $e) {
            throw new Exception("It's not posible to retrive additional information about external service" . $e->getMessage());
        }
    }

    /**
     * @param AsNewsPlayer|null $asNewsPlayer
     * @param Person $person
     */
    protected function setPlayerAsNewsExternalAttributes(?AsNewsPlayer $asNewsPlayer, Person &$person): void
    {
        if (!empty($asNewsPlayer)) {
            $person->setTag($asNewsPlayer->getTag());
            $person->setSlug($asNewsPlayer->getSlug());
            $name = $person->getName();
            if (is_null($name)) {
                $name = new PersonName();
            }
            $internal = $name->getInternal();
            if (is_null($internal)) {
                $internal = new PersonInternal();
            }
            $internal->setKnown($asNewsPlayer->getShortName());
            $name->setInternal($internal);
            $person->setName($name);
            if (!empty($asNewsPlayer->getImage())) {
                $person->setImageId($asNewsPlayer->getImage()->getId());
            }
            if (!empty($asNewsPlayer->getPiece())) {
                $person->setUrl($asNewsPlayer->getPiece()->getId());
            }
        }
    }

    /**
     * @param AsNewsTeam|null $asNewsTeam
     * @param Team $team
     */
    protected function setTeamAsNewsExternalAttributes(?AsNewsTeam $asNewsTeam, Team &$team): void
    {
        if (!empty($asNewsTeam)) {
            $team->setTag($asNewsTeam->getTag());
            $team->setSlug($asNewsTeam->getSlug());
            $name = $team->getName();
            if (is_null($name)) {
                $name = new TeamName();
            }
            $internal = $name->getInternal();
            if (is_null($internal)) {
                $internal = new TeamInternal();
            }
            $internal->setKnown($asNewsTeam->getShortName());
            $internal->setAbbreviation($asNewsTeam->getAbbreviation());
            $name->setInternal($internal);
            $team->setName($name);
            if (!is_null($asNewsTeam->getPiece()) && !empty($asNewsTeam->getPiece()->getId())) {
                $team->setUrl($asNewsTeam->getPiece()->getId());
            }
            if (!empty($asNewsTeam->getImage())) {
                $team->setImageId($asNewsTeam->getImage()->getId());
            }
        }
    }

    /**
     * @param AsNewsTv|null $asNewsTv
     * @param Tv $tv
     */
    protected function setTvAsNewsExternalAttributes(?AsNewsTv $asNewsTv, Tv &$tv): void
    {
        if (!empty($asNewsTv)) {
            $tv->setImageId($asNewsTv->getId());
        }
    }

    /**
     * @param AsNewsMatch|null $asNewsMatch
     * @param Match $match
     */
    protected function setMatchAsNewsExternalAttributes(?AsNewsMatch $asNewsMatch, Match &$match): void
    {
        if (!empty($asNewsMatch)) {
            $url = $asNewsMatch->getUrl();
            if (!empty($url)) {
                $urlInsert = new Url();
                $urlInsert->setMatch($url->getMatch());
                $news = $url->getNews();
                if (!is_null($news)) {
                    $newsInsert = [];
                    foreach ($news as $newsValue) {
                        $news = new News();
                        $news->setEditionSlug($newsValue->getEditionSlug());
                        $news->setPreMatch($newsValue->getPreMatch());
                        $news->setLive($newsValue->getLive());
                        $news->setFinished($newsValue->getFinished());
                        $newsInsert[] = $news;
                    }
                    $urlInsert->setNews($newsInsert);
                }
                $match->setUrl($urlInsert);
            }
        }
    }
}