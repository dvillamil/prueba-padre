<?php

namespace App\Utils;

use DateTime;
use DateTimeZone;
use Exception;

class Date extends DateTime
{
    public const FORMAT_Ymd = 'Y-m-d';

    private const EVERYTHING_0 = '/0+[^\d]{1}0+[^\d]{1}0+/';

    /**
     * Date constructor.
     * @param string $date
     * @param DateTimeZone|null $timezone
     * @throws Exception
     */
    public function __construct(string $date, DateTimeZone $timezone = null)
    {
        $time = strtotime($date);
        if ($time === false || $this->checkEverythingZero($date)) {
            throw new Exception('Invalid date: ' . $date);
        }
        parent::__construct($date, $timezone);
    }

    /**
     * @param string $date
     * @return bool
     */
    private function checkEverythingZero(string $date): bool
    {
        return (bool)preg_match(self::EVERYTHING_0, $date);
    }
}