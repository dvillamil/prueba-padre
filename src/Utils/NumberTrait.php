<?php

namespace App\Utils;

use Exception;

trait NumberTrait
{
    /**
     * @param string $letter
     * @return int
     * @throws Exception
     */
    public function letterToNumber(string $letter): int
    {
        $letter = strtoupper($letter);
        $range = range('A', 'Z');
        $result = array_search($letter, $range);
        if ($result === false) {
            throw new Exception('Transformation has failed. Letter not found: ' . $letter);
        }
        return $result + 1;
    }

    /**
     * @param int $number
     * @param bool $upperCaseReturn
     * @return string
     * @throws Exception
     */
    public function numberToLetter(int $number, bool $upperCaseReturn = false): string
    {
        //This is because in letter to number we add 1
        $number--;
        $range = range('a', 'z');
        if (!isset($range[$number])) {
            throw new Exception('Transformation has failed. Number has not have a letter assigned: ' . $number);
        }
        $result = $range[$number];
        if ($upperCaseReturn === true) {
            $result = strtoupper($result);
        }
        return $result;
    }

    /**
     * Returns whether a value is empty but not 0
     * @param $data
     * @return bool
     */
    public function emptyNotZero($data): bool
    {
        return (empty($data) && $data !== "0" && $data !== 0);
    }
}