<?php

namespace App\Utils;

trait StringTrait
{
    /**
     * @param string $string
     * @return string
     */
    public function normalizeString(string $string): string
    {
        $normString = trim($string);
        $normString = mb_strtolower($normString);
        $pattern = array(
            'á',
            'à',
            'â',
            'ä',
            'é',
            'è',
            'ê',
            'ë',
            'í',
            'î',
            'î',
            'ï',
            'ó',
            'ò',
            'ô',
            'ö',
            'ú',
            'ù',
            'û',
            'ü',
            'ñ',
            'ç'
        );
        $replacement = array(
            'a',
            'a',
            'a',
            'a',
            'e',
            'e',
            'e',
            'e',
            'i',
            'i',
            'i',
            'i',
            'o',
            'o',
            'o',
            'o',
            'u',
            'u',
            'u',
            'u',
            'n',
            'c'
        );
        $normString = str_replace($pattern, $replacement, $normString);
        $normString = preg_replace('/[\s-]/', '_', $normString);
        $normString = preg_replace('/_+/', '_', $normString);
        $normString = preg_replace('/[^a-z0-9_]/', '', $normString);
        return $normString;
    }
}
