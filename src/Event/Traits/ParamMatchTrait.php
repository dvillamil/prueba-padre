<?php

namespace App\Event\Traits;

trait ParamMatchTrait
{
    /**
     * @var string|null
     */
    protected $match;

    /**
     * @return string|null
     */
    public function getMatch(): ?string
    {
        return $this->match;
    }

    /**
     * @param string|null $match
     */
    public function setMatch(?string $match): void
    {
        $this->match = $match;
    }
}