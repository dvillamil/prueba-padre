<?php

namespace App\Event\Traits;

trait ParamSeasonTrait
{
    /**
     * @var integer|null
     */
    protected $season;

    /**
     * @return int|null
     */
    public function getSeason(): ?int
    {
        return $this->season;
    }

    /**
     * @param int|null $season
     */
    public function setSeason(?int $season): void
    {
        $this->season = $season;
    }
}