<?php

namespace App\Event\Traits;

trait ParamCompetitionSeasonTrait
{
    /**
     * @var string|null
     */
    protected $competition_season;

    /**
     * @return string|null
     */
    public function getCompetitionSeason(): ?string
    {
        return $this->competition_season;
    }

    /**
     * @param string|null $competition_season
     */
    public function setCompetitionSeason(?string $competition_season): void
    {
        $this->competition_season = $competition_season;
    }
}