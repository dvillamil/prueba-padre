<?php

namespace App\Event\Traits;

trait ParamMatchDayTrait
{
    /**
     * @var integer|null
     */
    protected $match_day;

    /**
     * @return int|null
     */
    public function getMatchDay(): ?int
    {
        return $this->match_day;
    }

    /**
     * @param int|null $match_day
     */
    public function setMatchDay(?int $match_day): void
    {
        $this->match_day = $match_day;
    }
}