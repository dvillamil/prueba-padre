<?php

namespace App\Event\Traits;

trait ParamTeamTrait
{
    /**
     * @var string|null
     */
    protected $team;

    /**
     * @return string|null
     */
    public function getTeam(): ?string
    {
        return $this->team;
    }

    /**
     * @param string|null $team
     */
    public function setTeam(?string $team): void
    {
        $this->team = $team;
    }
}