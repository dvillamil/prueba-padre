<?php

namespace App\Event\ResourceProcessed\Ranking;

use App\Event\ResourceProcessed\AbstractResourceProcessedEvent;
use App\Event\Traits\ParamCompetitionSeasonTrait;

abstract class RankingEvent extends AbstractResourceProcessedEvent
{
    protected const RESOURCE_TYPE = 'ranking';

    use ParamCompetitionSeasonTrait;
}