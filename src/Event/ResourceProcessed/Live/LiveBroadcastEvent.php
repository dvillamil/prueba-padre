<?php

namespace App\Event\ResourceProcessed\Live;

class LiveBroadcastEvent extends LiveEvent
{
    private const RESOURCE_SUBTYPE = 'BROADCAST';

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, self::RESOURCE_SUBTYPE);
    }
}