<?php

namespace App\Event\ResourceProcessed\Live;

use App\Event\ResourceProcessed\AbstractResourceProcessedEvent;
use App\Event\Traits\ParamMatchTrait;

abstract class LiveEvent extends AbstractResourceProcessedEvent
{
    protected const RESOURCE_TYPE = 'LIVE';

    use ParamMatchTrait;
}