<?php

namespace App\Event\ResourceProcessed\Roster;

class RosterTeamsEvent extends RosterEvent
{
    private const RESOURCE_SUBTYPE = 'TEAMS';

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, self::RESOURCE_SUBTYPE);
    }
}