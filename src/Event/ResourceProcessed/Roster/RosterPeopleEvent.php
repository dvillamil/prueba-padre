<?php

namespace App\Event\ResourceProcessed\Roster;

class RosterPeopleEvent extends RosterEvent
{
    private const RESOURCE_SUBTYPE = 'PEOPLE';

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, self::RESOURCE_SUBTYPE);
    }
}