<?php

namespace App\Event\ResourceProcessed\Team;

class TeamStatsEvent extends TeamEvent
{
    private const RESOURCE_SUBTYPE = 'STATS';

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, self::RESOURCE_SUBTYPE);
    }
}