<?php

namespace App\Event\ResourceProcessed\Team;

class TeamCalendarEvent extends TeamEvent
{
    private const RESOURCE_SUBTYPE = 'CALENDAR';

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, self::RESOURCE_SUBTYPE);
    }
}