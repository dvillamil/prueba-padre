<?php

namespace App\Event\ResourceProcessed\Team;

class TeamSquadEvent extends TeamEvent
{
    private const RESOURCE_SUBTYPE = 'SQUAD';

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, self::RESOURCE_SUBTYPE);
    }
}