<?php

namespace App\Event\ResourceProcessed\Carousel;

use App\Event\ResourceProcessed\AbstractResourceProcessedEvent;
use App\Event\Traits\ParamCompetitionSeasonTrait;
use App\Event\Traits\ParamMatchDayTrait;
use App\Event\Traits\ParamStageTrait;

class CarouselEvent extends AbstractResourceProcessedEvent
{
    protected const RESOURCE_TYPE = 'CAROUSEL';

    use ParamCompetitionSeasonTrait;
    use ParamStageTrait;
    use ParamMatchDayTrait;

    public function __construct()
    {
        parent::__construct(self::RESOURCE_TYPE, null);
    }
}