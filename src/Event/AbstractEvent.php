<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

abstract class AbstractEvent extends Event
{
    /**
     * @return string
     */
    public abstract function getEventName(): string;
}